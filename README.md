# MonoShh-Fitting


## Getting setup
# First time setup - HistFitter
To setup HistFitter, navigate to the cloned monoshh-fitting repository and run:
```
setupATLAS
mkdir build install run
lsetup "views LCG_104a x86_64-centos7-gcc12-opt" # or lsetup "views LCG_104a x86_64-el9-gcc12-opt"
cd build
cmake ../HistFitter/ -B. -DCMAKE_INSTALL_PREFIX=../install
make -j2 install
cd ..
source install/bin/setup_histfitter.sh
```
This has compiled the HistFitter code and created the relevant directories required to run our code.

# Not First Time
If you have compiled HistFitter before, please use the `setup_el9.sh` or `setup_centos7.sh` corresponding to your needs.

# Running the code
To run a background-only fit:
```
# inside monoshh-fitting repository
HistFitter.py -d -t -w -f -L "INFO" -F bkg python/multipleSRs_monoShh.py ++signalSample monoShhall_zp1700_dm200_dh310 > logs/log.log 2> logs/err.err
```
This example runs a background-only fit, where normalisation factors for `$$t\bar{t}+light$$`, `$$t\bar{t}+>1b$$` and `Z+jets` backgrounds are fit to data in the four control regions: `CR_4b`, `CR_3b_low_mTb`, `CR_3b_high_mTb`, `CR_Z`. Nuisance parameters corresponding to the systematic uncertainties for all background processes are profiled, with the fit maximising the likelihood function - constructed as a product of poisson distributions for each region.

Flags:
- `-t`: re-create histograms from TTrees
- `-w`: re-create workspace from histograms
- `-f`: fit the workspace
- `-L`: level of information to log (VERBOSE, DEBUG, INFO, WARNING, ERROR, FATAL, ALWAYS)
- `-d`: plot pre/post-fit CR distributions

Additional flag:
`++doSyst` - Run with full systematics, without this flag we run with large theory systematics 

To run an exclusion-fit:
```
HistFitter.py -a -t -w -f -p -L "INFO" -F excl python/multipleSRs_monoShh.py ++signalSample monoShhall_zp1700_dm200_dh310 ++doToys > logs/exclusion_fit.log 2> logs/exclusion_fit.err
```
Flags:
- `-a`: fit on asimov dataset, using pre-fit background yields as the dataset, allowing only systematic constraints to occur

- `-p`: perform an hypothesis test of workspace

Additional flag:
- `++doToys`: use toy experiments rather than asymptotic distributions for the test-statistic (NOTE: buggy at the moment, segmentation faults.)
