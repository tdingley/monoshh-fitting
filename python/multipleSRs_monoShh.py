"""
 **********************************************************************************
 * Project: HistFitter - A ROOT-based package for statistical data analysis       *
 * Package: HistFitter                                                            *
 *                                                                                *
 * Description:                                                                   *
 *      Simple example configuration using a single bin and raw numbers           * 
 *                                                                                *
 * Authors:                                                                       *
 *      HistFitter group, CERN, Geneva                                            *
 *                                                                                *
 * Redistribution and use in source and binary forms, with or without             *
 * modification, are permitted according to the terms listed in the file          *
 * LICENSE.                                                                       *
 **********************************************************************************
"""

################################################################
## In principle all you have to setup is defined in this file ##
################################################################
import json
import os
import re
import time
from math import sqrt
import numpy as np

start_time = time.time()

import ROOT
from configManager import configMgr
from configWriter import Channel, Measurement, Sample, fitConfig
from ROOT import (gROOT, kAzure, kBlack, kBlue, kCyan, kGray, kGreen, kMagenta,
                  kOrange, kPink, kRed, kSpring, kTeal, kViolet, kWhite,
                  kYellow)
from systematic import Systematic


def replaceWeight(weights, pattern, replace):
  return [replace if w == pattern else w for w in weights]
from argparse import ArgumentParser


parser = ArgumentParser(description="MBJ HistFitter options:", prefix_chars='+')
parser.add_argument('++doToys',action='store_true')
parser.add_argument('++doSyst',                 action='store_true')
parser.add_argument('++signalSample',           type=str, required=bool(configMgr.myFitType == configMgr.FitType.Exclusion), help='Signal region to run over for exclusion')
parser.add_argument('++doPlots', action='store_true')
parser.add_argument('++addString', default = '',type=str, help="You can add a string to teh results dir if you wish")
parser.add_argument('++diffCR', action='store_true')

# read in arguments
args, _ = parser.parse_known_args()
mass = args.signalSample
doToys = args.doToys
doExp_syst = args.doSyst
doPlots = args.doPlots
add_string = args.addString
diffCR = args.diffCR
print("Arguments: ", mass, doToys, doExp_syst)

# define all input files:
inputFiles = {
    "/data/atlas/users/bruckler/production_21p2p129/filtered_newGrid/Data_21.2.129_15.data_10sel_optimisedSelectionFiltered_defined10_ID_withNNScore_4096_disCo_train_set_and_additional_events_allb_inclZCR_.root",
    "/data/atlas/users/bruckler/production_21p2p129/filtered_newGrid/Data_21.2.129_16.data_10sel_optimisedSelectionFiltered_defined10_ID_withNNScore_4096_disCo_train_set_and_additional_events_allb_inclZCR_.root",
    "/data/atlas/users/bruckler/production_21p2p129/filtered_newGrid/Data_21.2.129_17.data_10sel_optimisedSelectionFiltered_defined10_ID_withNNScore_4096_disCo_train_set_and_additional_events_allb_inclZCR_.root",
    "/data/atlas/users/bruckler/production_21p2p129/filtered_newGrid/Data_21.2.129_18_1.data_10sel_optimisedSelectionFiltered_defined10_ID_withNNScore_4096_disCo_train_set_and_additional_events_allb_inclZCR_.root",
    "/data/atlas/users/bruckler/production_21p2p129/filtered_newGrid/Data_21.2.129_18_2.data_10sel_optimisedSelectionFiltered_defined10_ID_withNNScore_4096_disCo_train_set_and_additional_events_allb_inclZCR_.root",
    "/data/atlas/users/bruckler/production_21p2p129/filtered_newGrid/Sig_21.2.129_mc16a.monoShh_newGrid_allRounds_optimisedSelectionFiltered_defined10_ID_withNNScore_4096_disCo_train_set_and_additional_events_allb_inclZCR_monoShhall_allSignals.root",
    "/data/atlas/users/bruckler/production_21p2p129/filtered_newGrid/Sig_21.2.129_mc16d.monoShh_newGrid_allRounds_optimisedSelectionFiltered_defined10_ID_withNNScore_4096_disCo_train_set_and_additional_events_allb_inclZCR_monoShhall_allSignals.root",
    "/data/atlas/users/bruckler/production_21p2p129/filtered_newGrid/Sig_21.2.129_mc16e.monoShh_newGrid_allRounds_optimisedSelectionFiltered_defined10_ID_withNNScore_4096_disCo_train_set_and_additional_events_allb_inclZCR_monoShhall_allSignals.root",
    "/data/atlas/users/bruckler/production_21p2p129/filtered_newGrid/Bkg_21.2.129_mc16a.diboson_sys_optimisedSelectionFiltered_defined10_ID_withNNScore_4096_disCo_train_set_and_additional_events_allb_inclZCR_.root",
    "/data/atlas/users/bruckler/production_21p2p129/filtered_newGrid/Bkg_21.2.129_mc16a.singletop_sys_optimisedSelectionFiltered_defined10_ID_withNNScore_4096_disCo_train_set_and_additional_events_allb_inclZCR_.root",
    "/data/atlas/users/bruckler/production_21p2p129/filtered_newGrid/Bkg_21.2.129_mc16a.topEW_sys_optimisedSelectionFiltered_defined10_ID_withNNScore_4096_disCo_train_set_and_additional_events_allb_inclZCR_fixed.root",
    "/data/atlas/users/bruckler/production_21p2p129/filtered_newGrid/Bkg_21.2.129_mc16a.triboson_sys_optimisedSelectionFiltered_defined10_ID_withNNScore_4096_disCo_train_set_and_additional_events_allb_inclZCR_.root",
    "/data/atlas/users/bruckler/production_21p2p129/filtered_newGrid/Bkg_21.2.129_mc16a.ttbar_sys_optimisedSelectionFiltered_defined10_ID_withNNScore_4096_disCo_train_set_and_additional_events_allb_inclZCR_.root",
    "/data/atlas/users/bruckler/production_21p2p129/filtered_newGrid/Bkg_21.2.129_mc16a.VH_sys_optimisedSelectionFiltered_defined10_ID_withNNScore_4096_disCo_train_set_and_additional_events_allb_inclZCR_.root",
    "/data/atlas/users/bruckler/production_21p2p129/filtered_newGrid/Bkg_21.2.129_mc16a.W_jets_sys_optimisedSelectionFiltered_defined10_ID_withNNScore_4096_disCo_train_set_and_additional_events_allb_inclZCR_.root",
    "/data/atlas/users/bruckler/production_21p2p129/filtered_newGrid/Bkg_21.2.129_mc16a.Z_jets_sys_optimisedSelectionFiltered_defined10_ID_withNNScore_4096_disCo_train_set_and_additional_events_allb_inclZCR_.root",
    "/data/atlas/users/bruckler/production_21p2p129/filtered_newGrid/Bkg_21.2.129_mc16d.diboson_sys_optimisedSelectionFiltered_defined10_ID_withNNScore_4096_disCo_train_set_and_additional_events_allb_inclZCR_.root",
    "/data/atlas/users/bruckler/production_21p2p129/filtered_newGrid/Bkg_21.2.129_mc16d.singletop_sys_optimisedSelectionFiltered_defined10_ID_withNNScore_4096_disCo_train_set_and_additional_events_allb_inclZCR_.root",
    "/data/atlas/users/bruckler/production_21p2p129/filtered_newGrid/Bkg_21.2.129_mc16d.topEW_sys_optimisedSelectionFiltered_defined10_ID_withNNScore_4096_disCo_train_set_and_additional_events_allb_inclZCR_.root",
    "/data/atlas/users/bruckler/production_21p2p129/filtered_newGrid/Bkg_21.2.129_mc16d.triboson_sys_optimisedSelectionFiltered_defined10_ID_withNNScore_4096_disCo_train_set_and_additional_events_allb_inclZCR_.root",
    "/data/atlas/users/bruckler/production_21p2p129/filtered_newGrid/Bkg_21.2.129_mc16d.ttbar_sys_optimisedSelectionFiltered_defined10_ID_withNNScore_4096_disCo_train_set_and_additional_events_allb_inclZCR_.root",
    "/data/atlas/users/bruckler/production_21p2p129/filtered_newGrid/Bkg_21.2.129_mc16d.VH_sys_optimisedSelectionFiltered_defined10_ID_withNNScore_4096_disCo_train_set_and_additional_events_allb_inclZCR_.root",
    "/data/atlas/users/bruckler/production_21p2p129/filtered_newGrid/Bkg_21.2.129_mc16d.W_jets_sys_optimisedSelectionFiltered_defined10_ID_withNNScore_4096_disCo_train_set_and_additional_events_allb_inclZCR_.root",
    "/data/atlas/users/bruckler/production_21p2p129/filtered_newGrid/Bkg_21.2.129_mc16d.Z_jets_sys_optimisedSelectionFiltered_defined10_ID_withNNScore_4096_disCo_train_set_and_additional_events_allb_inclZCR_.root",
    "/data/atlas/users/bruckler/production_21p2p129/filtered_newGrid/Bkg_21.2.129_mc16e.diboson_sys_optimisedSelectionFiltered_defined10_ID_withNNScore_4096_disCo_train_set_and_additional_events_allb_inclZCR_.root",
    "/data/atlas/users/bruckler/production_21p2p129/filtered_newGrid/Bkg_21.2.129_mc16e.singletop_sys_optimisedSelectionFiltered_defined10_ID_withNNScore_4096_disCo_train_set_and_additional_events_allb_inclZCR_.root",
    "/data/atlas/users/bruckler/production_21p2p129/filtered_newGrid/Bkg_21.2.129_mc16e.topEW_sys_optimisedSelectionFiltered_defined10_ID_withNNScore_4096_disCo_train_set_and_additional_events_allb_inclZCR_.root",
    "/data/atlas/users/bruckler/production_21p2p129/filtered_newGrid/Bkg_21.2.129_mc16e.triboson_sys_optimisedSelectionFiltered_defined10_ID_withNNScore_4096_disCo_train_set_and_additional_events_allb_inclZCR_.root",
    "/data/atlas/users/bruckler/production_21p2p129/filtered_newGrid/Bkg_21.2.129_mc16e.ttbar_sys_optimisedSelectionFiltered_defined10_ID_withNNScore_4096_disCo_train_set_and_additional_events_allb_inclZCR_.root",
    "/data/atlas/users/bruckler/production_21p2p129/filtered_newGrid/Bkg_21.2.129_mc16e.VH_sys_optimisedSelectionFiltered_defined10_ID_withNNScore_4096_disCo_train_set_and_additional_events_allb_inclZCR_.root",
    "/data/atlas/users/bruckler/production_21p2p129/filtered_newGrid/Bkg_21.2.129_mc16e.W_jets_sys_optimisedSelectionFiltered_defined10_ID_withNNScore_4096_disCo_train_set_and_additional_events_allb_inclZCR_.root",
    "/data/atlas/users/bruckler/production_21p2p129/filtered_newGrid/Bkg_21.2.129_mc16e.Z_jets_sys_optimisedSelectionFiltered_defined10_ID_withNNScore_4096_disCo_train_set_and_additional_events_allb_inclZCR_.root"
}
# define all backgrounds used for this fit, for now VH & Triboson left out (If added please update file list above)
samples = ["W_jets", "ttbar", "diboson", "Z_jets", "singletop", "topEW"] 

sampleColors = {
    "W_jets": "#00B050",
    "data": 1,
    "ttbar": 870,
    "ttlight": "#5499C7",
    "ttbb": "#E67E22", 
    "ttcc": "#5D6D7E",
    "diboson": "#396A92",
    "singletop": "#CD5C5C",
    "topEW": "#FFC000",
    "Z_jets": "#DA70D6",
    "VH": "#ACACAC",
    "triboson": "#80CDE8"
}

def getSampleColor(sample):
    return sampleColors.get(sample, 1) if isinstance(sampleColors.get(sample, 1), int) else ROOT.TColor.GetColor(sampleColors.get(sample, 1))

#-------------------------------
# Parameters for hypothesis test
#-------------------------------
#configMgr.doHypoTest=False
configMgr.nTOYs=1000

# 2 = asymptotics calc, 0 = toys, 3 = asimov, 1 = hybrid calculator
if doToys:
    print(f'Running {configMgr.nTOYs} Toys')
    configMgr.calculatorType=0
else:
    print("Running asymptotics with asimov dataset")
    configMgr.calculatorType=3

# oneside PRL - LHC Recommendation
configMgr.testStatType=3

# number of values scanned of signal-strength for upper-limit determination of signal strength.
configMgr.nPoints=20
configMgr.writeXML = False


#-------------------------------------
# Now we start to build the data model
#-------------------------------------
# First we define our analysisname, used for the results directory
if configMgr.myFitType == configMgr.FitType.Background:
    configMgr.analysisName = "Bkg_fit"
    if doExp_syst:
        configMgr.analysisName += "_allsyst"
    else:
        configMgr.analysisName += "_nosyst"
elif configMgr.myFitType == configMgr.FitType.Exclusion:
    print("We're running exclusion fit, make the analysis name accordingly")
    configMgr.analysisName = "Exclusion"
    if doToys:
        configMgr.analysisName += "_toys"
    else:
        configMgr.analysisName += "_asym"
    if doExp_syst:
        configMgr.analysisName += "_allsyst"
    else:
        configMgr.analysisName += "_nosyst"
elif configMgr.myFitType == configMgr.FitType.Discovery:
    configMgr.analysisName = "discoveryfit"
else:
    raise ValueError("We need to make sure we're using one of the three supported fit types")


doMonoShh = False
if doMonoShh == False:
    configMgr.analysisName += "_multib_"
elif doMonoShh:
    configMgr.analysisName += "_monoShh"
else: 
    raise ValueError("Logic for analysisname not quite right, check code.")

configMgr.analysisName += args.signalSample
configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"
# which dir to store fit file
configMgr.outputFileName = "results/"+configMgr.analysisName+"_Output.root"
configMgr.hitsCacheFile = f"cache/results/_{configMgr.analysisName}.root"
configMgr.useCacheToTreeFallback = True
configMgr.useHistBackupCacheFile = True

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 1 # Luminosity of input TTree after weighting
configMgr.outputLumi = 1 # Luminosity required for output histograms
configMgr.setLumiUnits("fb-1")
configMgr.blindSR=True
configMgr.useSignalInBlindedData=False
# the cross section unc for signal are not treated as normal uncertainties
configMgr.fixSigXSec = True
# Set the files to read from
# hard code to read from trees to start
configMgr.readFromTree = True

# prun small systematics
configMgr.prun = True

# at which threshold ( % from the nominal ) should we disable systematic, default 0.01
configMgr.prunThreshold = 0.02
configMgr.nomName = "_nominal"
configMgr.ReduceCorrMatrix = True


print("TOM:",inputFiles)
if configMgr.readFromTree:
    bkgFiles = [file_path for file_path in inputFiles if "bkg_" in os.path.basename(file_path).lower()]
    print("TOM: checking me logic: ", [file_path for file_path in inputFiles])
    dataFiles =  [file_path for file_path in inputFiles if "data_" in os.path.basename(file_path).lower()]
    sigFiles = [file_path for file_path in inputFiles if "sig" in os.path.basename(file_path).lower()]
    print(len(bkgFiles))
    print(len(dataFiles))
    print(len(sigFiles))
else:
    bkgFiles = [configMgr.histCacheFile]
    pass

# for debugging purposes:
print("DEBUG: each file array: ", bkgFiles)
print(dataFiles)
print(sigFiles)


# Dictionnary of cuts for Tree->hist
configMgr.cutsDict["SR_monoShh_bin_0_50"] = "(pass_MET == 1) && (signal_electrons_n == 0) && (signal_muons_n == 0) && (met >= 200) && (4 <= jets_n) && (jets_n <= 7) && (bjets_n >= 3) && (dphi_min > 0.4) && (metsig_obj > 7.) && (0.95 < NNScore) && ( NNScore<= 1)"
configMgr.cutsDict["SR_monoShh_bin_50_100"] = "(pass_MET == 1) && (signal_electrons_n == 0) && (signal_muons_n == 0) && (met >= 200) && (4 <= jets_n) && (jets_n <= 7) && (bjets_n >= 3) && (dphi_min > 0.4) && (metsig_obj > 7.) && (0.95 < NNScore) && ( NNScore<= 1)"
configMgr.cutsDict["SR_monoShh_bin_100_150"] = "(pass_MET == 1) && (signal_electrons_n == 0) && (signal_muons_n == 0) && (met >= 200) && (4 <= jets_n) && (jets_n <= 7) && (bjets_n >= 3) && (dphi_min > 0.4) && (metsig_obj > 7.) && (0.95 < NNScore) && ( NNScore<= 1)"
configMgr.cutsDict["SR_monoShh_bin_100_150"] = "(pass_MET == 1) && (signal_electrons_n == 0) && (signal_muons_n == 0) && (met >= 200) && (4 <= jets_n) && (jets_n <= 7) && (bjets_n >= 3) && (dphi_min > 0.4) && (metsig_obj > 7.) && (0.95 < NNScore) && ( NNScore<= 1)"
configMgr.cutsDict["SR_monoShh_bin_150_200"] = "(pass_MET == 1) && (signal_electrons_n == 0) && (signal_muons_n == 0) && (met >= 200) && (4 <= jets_n) && (jets_n <= 7) && (bjets_n >= 3) && (dphi_min > 0.4) && (metsig_obj > 7.) && (0.95 < NNScore) && ( NNScore<= 1)"
configMgr.cutsDict["SR_monoShh_bin_200_250"] = "(pass_MET == 1) && (signal_electrons_n == 0) && (signal_muons_n == 0) && (met >= 200) && (4 <= jets_n) && (jets_n <= 7) && (bjets_n >= 3) && (dphi_min > 0.4) && (metsig_obj > 7.) && (0.95 < NNScore) && ( NNScore<= 1)"
configMgr.cutsDict["SR_monoShh_bin_250_300"] = "(pass_MET == 1) && (signal_electrons_n == 0) && (signal_muons_n == 0) && (met >= 200) && (4 <= jets_n) && (jets_n <= 7) && (bjets_n >= 3) && (dphi_min > 0.4) && (metsig_obj > 7.) && (0.95 < NNScore) && ( NNScore<= 1)"
configMgr.cutsDict["SR_monoShh_bin_300_350"] = "(pass_MET == 1) && (signal_electrons_n == 0) && (signal_muons_n == 0) && (met >= 200) && (4 <= jets_n) && (jets_n <= 7) && (bjets_n >= 3) && (dphi_min > 0.4) && (metsig_obj > 7.) && (0.95 < NNScore) && ( NNScore<= 1)"
configMgr.cutsDict["SR_monoShh_bin_350_400"] = "(pass_MET == 1) && (signal_electrons_n == 0) && (signal_muons_n == 0) && (met >= 200) && (4 <= jets_n) && (jets_n <= 7) && (bjets_n >= 3) && (dphi_min > 0.4) && (metsig_obj > 7.) && (0.95 < NNScore) && ( NNScore<= 1)"
configMgr.cutsDict["SR_monoShh_bin_400_450"] = "(pass_MET == 1) && (signal_electrons_n == 0) && (signal_muons_n == 0) && (met >= 200) && (4 <= jets_n) && (jets_n <= 7) && (bjets_n >= 3) && (dphi_min > 0.4) && (metsig_obj > 7.) && (0.95 < NNScore) && ( NNScore<= 1)"
configMgr.cutsDict["SR_monoShh_bin_450_500"] = "(pass_MET == 1) && (signal_electrons_n == 0) && (signal_muons_n == 0) && (met >= 200) && (4 <= jets_n) && (jets_n <= 7) && (bjets_n >= 3) && (dphi_min > 0.4) && (metsig_obj > 7.) && (0.95 < NNScore) && ( NNScore<= 1)"
configMgr.cutsDict["SR_monoShh_bin_500_550"] = "(pass_MET == 1) && (signal_electrons_n == 0) && (signal_muons_n == 0) && (met >= 200) && (4 <= jets_n) && (jets_n <= 7) && (bjets_n >= 3) && (dphi_min > 0.4) && (metsig_obj > 7.) && (0.95 < NNScore) && ( NNScore<= 1)"
configMgr.cutsDict["SR_monoShh_bin_550_600"] = "(pass_MET == 1) && (signal_electrons_n == 0) && (signal_muons_n == 0) && (met >= 200) && (4 <= jets_n) && (jets_n <= 7) && (bjets_n >= 3) && (dphi_min > 0.4) && (metsig_obj > 7.) && (0.95 < NNScore) && ( NNScore<= 1)"
configMgr.cutsDict["SR_monoShh_bin_600_650"] = "(pass_MET == 1) && (signal_electrons_n == 0) && (signal_muons_n == 0) && (met >= 200) && (4 <= jets_n) && (jets_n <= 7) && (bjets_n >= 3) && (dphi_min > 0.4) && (metsig_obj > 7.) && (0.95 < NNScore) && ( NNScore<= 1)"
configMgr.cutsDict["SR_monoShh_bin_650_700"] = "(pass_MET == 1) && (signal_electrons_n == 0) && (signal_muons_n == 0) && (met >= 200) && (4 <= jets_n) && (jets_n <= 7) && (bjets_n >= 3) && (dphi_min > 0.4) && (metsig_obj > 7.) && (0.95 < NNScore) && ( NNScore<= 1)"
configMgr.cutsDict["SR_monoShh_bin_700_750"] = "(pass_MET == 1) && (signal_electrons_n == 0) && (signal_muons_n == 0) && (met >= 200) && (4 <= jets_n) && (jets_n <= 7) && (bjets_n >= 3) && (dphi_min > 0.4) && (metsig_obj > 7.) && (0.95 < NNScore) && ( NNScore<= 1)"
configMgr.cutsDict["SR_monoShh_bin_750_800"] = "(pass_MET == 1) && (signal_electrons_n == 0) && (signal_muons_n == 0) && (met >= 200) && (4 <= jets_n) && (jets_n <= 7) && (bjets_n >= 3) && (dphi_min > 0.4) && (metsig_obj > 7.) && (0.95 < NNScore) && ( NNScore<= 1)"
configMgr.cutsDict["SR_monoShh_bin_750_800"] = "(pass_MET == 1) && (signal_electrons_n == 0) && (signal_muons_n == 0) && (met >= 200) && (4 <= jets_n) && (jets_n <= 7) && (bjets_n >= 3) && (dphi_min > 0.4) && (metsig_obj > 7.) && (0.95 < NNScore) && ( NNScore<= 1)"
configMgr.cutsDict["SR_monoShh_bin_800_850"] = "(pass_MET == 1) && (signal_electrons_n == 0) && (signal_muons_n == 0) && (met >= 200) && (4 <= jets_n) && (jets_n <= 7) && (bjets_n >= 3) && (dphi_min > 0.4) && (metsig_obj > 7.) && (0.95 < NNScore) && ( NNScore<= 1)"
configMgr.cutsDict["SR_monoShh_bin_850_900"] = "(pass_MET == 1) && (signal_electrons_n == 0) && (signal_muons_n == 0) && (met >= 200) && (4 <= jets_n) && (jets_n <= 7) && (bjets_n >= 3) && (dphi_min > 0.4) && (metsig_obj > 7.) && (0.95 < NNScore) && ( NNScore<= 1)"
configMgr.cutsDict["SR_monoShh_bin_900_950"] = "(pass_MET == 1) && (signal_electrons_n == 0) && (signal_muons_n == 0) && (met >= 200) && (4 <= jets_n) && (jets_n <= 7) && (bjets_n >= 3) && (dphi_min > 0.4) && (metsig_obj > 7.) && (0.95 < NNScore) && ( NNScore<= 1)"
configMgr.cutsDict["SR_monoShh_bin_950_1000"] = "(pass_MET == 1) && (signal_electrons_n == 0) && (signal_muons_n == 0) && (met >= 200) && (4 <= jets_n) && (jets_n <= 7) && (bjets_n >= 3) && (dphi_min > 0.4) && (metsig_obj > 7.) && (0.95 < NNScore) && ( NNScore<= 1)"

# CR
if not(diffCR):
    configMgr.cutsDict["VR"] ="(pass_MET == 1) && (signal_electrons_n == 0) && (signal_muons_n == 0) && (met >= 200) && (4 <= jets_n) && (jets_n <= 7) && (bjets_n >= 3) && (dphi_min > 0.4) && (metsig_obj > 7.) && (0.85 < NNScore) && (NNScore<= 0.95)"
    configMgr.cutsDict["CR_4b"] = "(pass_MET == 1) && (signal_electrons_n == 0) && (signal_muons_n == 0) && (met >= 200) && (4 <= jets_n) && (jets_n <= 7) && (bjets_n >= 4) && (dphi_min > 0.4) && (metsig_obj > 7.) && (mTb_min > 80.) && (0.35 < NNScore) && ( NNScore<= 0.85)"
    configMgr.cutsDict["CR_3b_high_mTb"] = "(pass_MET == 1) && (signal_electrons_n == 0) && (signal_muons_n == 0) && (met >= 200) && (4 <= jets_n) && (jets_n <= 7) && (bjets_n == 3) && (dphi_min > 0.4) && (metsig_obj > 7.) && (mTb_min > 200.) && (0.35 < NNScore) && ( NNScore<= 0.85)"
    configMgr.cutsDict["CR_3b_low_mTb"] = "(pass_MET == 1) && (signal_electrons_n == 0) && (signal_muons_n == 0) && (met >= 200) && (4 <= jets_n) && (jets_n <= 7) && (bjets_n == 3) && (dphi_min > 0.4) && (metsig_obj > 7.) && (mTb_min < 200.) && (mTb_min > 80.) && (0.35 < NNScore) && ( NNScore<= 0.85)"
    configMgr.cutsDict["CR_Z"] = "(pass_MET == 1) && (signal_electrons_n == 0) && (signal_muons_n == 2) && (4 <= jets_n) && (jets_n <= 7) && (bjets_n >= 3) && (met <= 75) && (ZCR_met/1000. >= 200) && Z_OSLeps && (75 <= Z_mass) && (Z_mass <= 105) && (ZCR_metsig_estimate>7.) && (ZCR_mTb_min>100.)"
else:
    configMgr.cutsDict["VR"] = "(pass_MET == 1) && (signal_electrons_n == 0) && (signal_muons_n == 0) && (met >= 200) && (4 <= jets_n) && (jets_n <= 7) && (bjets_n >= 3) && (dphi_min > 0.4) && (metsig_obj > 7.) && (metsig_obj < 18.) && (mT_4b_met > 350.) && (mT_4b_met < 800.) && (mTb_min > 125.) && (mTb_min < 350.) && (dr_min_over_met > 0.001) && (dr_min_over_met < 0.012) && (0.85 < NNScore) && (NNScore<= 0.95)"
    configMgr.cutsDict["CR_4b"] = "(pass_MET == 1) && (signal_electrons_n == 0) && (signal_muons_n == 0) && (met >= 200) && (4 <= jets_n) && (jets_n <= 7) && (bjets_n >= 4) && (dphi_min > 0.4) && (metsig_obj > 7.) && (metsig_obj < 18.) && (mT_4b_met > 350.) && (mT_4b_met < 800.) && (mTb_min > 80.) && (mTb_min < 280.) && (dr_min_over_met > 0.001) && (0.35 < NNScore) && ( NNScore<= 0.85)"
    configMgr.cutsDict["CR_3b_high_mTb"] = "(pass_MET == 1) && (signal_electrons_n == 0) && (signal_muons_n == 0) && (met >= 200) && (4 <= jets_n) && (jets_n <= 7) && (bjets_n == 3) && (dphi_min > 0.4) && (metsig_obj > 7.) && (metsig_obj < 18.) && (mT_4b_met > 350.) && (mT_4b_met < 800.) && (mTb_min > 200.) && (mTb_min < 350.) && (dr_min_over_met > 0.001) && (0.35 < NNScore) && ( NNScore<= 0.85)"
    configMgr.cutsDict["CR_3b_low_mTb"] = "(pass_MET == 1) && (signal_electrons_n == 0) && (signal_muons_n == 0) && (met >= 200) && (4 <= jets_n) && (jets_n <= 7) && (bjets_n == 3) && (dphi_min > 0.4) && (metsig_obj > 7.) && (metsig_obj < 18.) && (mT_4b_met > 350.) && (mT_4b_met < 800.) && (mTb_min < 200.) && (mTb_min > 100.) && (dr_min_over_met > 0.001) && (0.35 < NNScore) && ( NNScore<= 0.85)"
    configMgr.cutsDict["CR_Z"] = "(pass_MET == 1) && (signal_electrons_n == 0) && (signal_muons_n == 2) && (4 <= jets_n) && (jets_n <= 7) && (bjets_n >= 3) && (met <= 75) && (ZCR_met/1000. >= 200) && Z_OSLeps && (75 <= Z_mass) && (Z_mass <= 105) && (ZCR_metsig_estimate>7.) && (ZCR_mTb_min>100.)"
    

if diffCR:
    configMgr.analysisName += "OldAnalysisRegions"
else:
    configMgr.analysisName += "NewCRs"

if doPlots:
    configMgr.analysisName += "Full_Plotting"
else:
    configMgr.analysisName += "noPlots"
print("TOM: CUTS DICTIONARY:",configMgr.cutsDict)
# based of the defined cuts, for convenience later we define a region array:
if configMgr.myFitType==configMgr.FitType.Background:
    regions = ["CR_4b", "CR_3b_low_mTb", "CR_3b_high_mTb", "CR_Z", "VR"]
else:
    regions = ["CR_4b", "CR_3b_low_mTb", "CR_3b_high_mTb", "CR_Z", "VR",
    "SR_monoShh_bin_0_50",
    "SR_monoShh_bin_50_100",
    "SR_monoShh_bin_100_150",
    "SR_monoShh_bin_150_200",
    "SR_monoShh_bin_200_250",
    "SR_monoShh_bin_250_300",
    "SR_monoShh_bin_300_350",
    "SR_monoShh_bin_350_400",
    "SR_monoShh_bin_400_450",
    "SR_monoShh_bin_450_500",
    "SR_monoShh_bin_500_550",
    "SR_monoShh_bin_550_600",
    "SR_monoShh_bin_600_650",
    "SR_monoShh_bin_650_700",
    "SR_monoShh_bin_700_750",
    "SR_monoShh_bin_750_800",
    "SR_monoShh_bin_800_850",
    "SR_monoShh_bin_850_900",
    "SR_monoShh_bin_900_950",
    "SR_monoShh_bin_950_1000"]

configMgr.analysisName += add_string

# Tuples of nominal weights without and with b-jet selection
configMgr.weights = (
    "weight_mc",
    "weight_lumi_real",
    "weight_btag",
    "weight_jvt",
    "weight_pu",
    "weight_elec",
    "weight_muon",
    "weight_WZ_2_2"
)

configMgr.signalWeights= ("1.0")

#-------------------------------------------
# List of samples and their plotting colours
#-------------------------------------------
bkg_samples = {}
for sample in samples:

    if "ttbar" in sample: 
        print("Skipping ttbar in main loop, add this in after :)")
        if doMonoShh:
            bkg_samples['ttlight'] = Sample('ttlight', getSampleColor('ttlight'))
            bkg_samples['ttlight'].setStatConfig(True)
            bkg_samples['ttlight'].setNormFactor("mu_ttlight", 1., 0., 5.)
            bkg_samples['ttlight'].additionalCuts = "ttbar_class == 0"
            bkg_samples['ttlight'].setNormRegions([("CR_Z", "cuts"), ("CR_4b", "cuts"), ("CR_3b_low_mTb", "cuts"), ("CR_3b_high_mTb", "cuts")])
            bkg_samples['ttlight'].overrideTreename = "ttbar"
            bkg_samples['ttlight'].addInputs([file_path for file_path in bkgFiles if "ttbar" in os.path.basename(file_path)])
            # ttbb, normalisation factor, no additional uncertainty
            bkg_samples['ttbb'] = Sample('ttbb', getSampleColor('ttbb'))
            bkg_samples['ttbb'].setStatConfig(True)
            bkg_samples['ttbb'].setNormFactor("mu_ttbb", 1., 0., 5.)
            bkg_samples['ttbb'].setNormRegions([("CR_Z", "cuts"), ("CR_4b", "cuts"), ("CR_3b_low_mTb", "cuts"), ("CR_3b_high_mTb", "cuts")])
            bkg_samples['ttbb'].overrideTreename = "ttbar"
            bkg_samples['ttbb'].additionalCuts = "ttbar_class > 0"
            bkg_samples['ttbb'].addInputs([file_path for file_path in bkgFiles if "ttbar" in os.path.basename(file_path)])
            # ttcc, only additional uncertainty considered
            bkg_samples['ttcc'] = Sample('ttcc', getSampleColor('ttcc'))
            bkg_samples['ttcc'].setStatConfig(True)
            #bkg_samples['ttcc'].setNormByTheory()
            bkg_samples['ttcc'].addSystematic(Systematic("ttcc_NormSyst", configMgr.weights, 1.5, 0.5, "user", "userOverallSys"))
            bkg_samples['ttcc'].additionalCuts = "ttbar_class < 0"
            bkg_samples['ttcc'].overrideTreename = "ttbar"
            bkg_samples['ttcc'].addInputs([file_path for file_path in bkgFiles if "ttbar" in os.path.basename(file_path)])
        else:
            bkg_samples['ttlight'] = Sample('ttlight', getSampleColor('ttlight'))
            bkg_samples['ttlight'].setStatConfig(True)
            bkg_samples['ttlight'].setNormFactor("mu_ttlight", 1., 0., 5.)
            bkg_samples['ttlight'].additionalCuts = "ttbar_class == 0"
            bkg_samples['ttlight'].setNormRegions([("CR_Z", "cuts"), ("CR_4b", "cuts"), ("CR_3b_low_mTb", "cuts"), ("CR_3b_high_mTb", "cuts")])
            bkg_samples['ttlight'].overrideTreename = "ttbar"
            bkg_samples['ttlight'].addInputs([file_path for file_path in bkgFiles if "ttbar" in os.path.basename(file_path)])
            # ttbb, normalisation factor, no additional uncertainty
            bkg_samples['ttbb'] = Sample('ttbb', getSampleColor('ttbb'))
            bkg_samples['ttbb'].setStatConfig(True)
            bkg_samples['ttbb'].setNormFactor("mu_ttbb", 1., 0., 5.)
            bkg_samples['ttbb'].setNormRegions([("CR_Z", "cuts"), ("CR_4b", "cuts"), ("CR_3b_low_mTb", "cuts"), ("CR_3b_high_mTb", "cuts")])
            bkg_samples['ttbb'].overrideTreename = "ttbar"
            bkg_samples['ttbb'].additionalCuts = "ttbar_class > 0"
            bkg_samples['ttbb'].addInputs([file_path for file_path in bkgFiles if "ttbar" in os.path.basename(file_path)])
            # ttcc, only additional uncertainty considered
            bkg_samples['ttcc'] = Sample('ttcc', getSampleColor('ttcc'))
            bkg_samples['ttcc'].setStatConfig(True)
            bkg_samples['ttcc'].addSystematic(Systematic("ttcc_NormSyst", configMgr.weights, 1.5, 0.5, "user", "userOverallSys"))
            bkg_samples['ttcc'].additionalCuts = "ttbar_class < 0"
            bkg_samples['ttcc'].overrideTreename = "ttbar"
            bkg_samples['ttcc'].addInputs([file_path for file_path in bkgFiles if "ttbar" in os.path.basename(file_path)])
        continue

    print(f"sample {sample}")

    bkg_samples[sample] = Sample(sample, getSampleColor(sample))
    bkg_samples[sample].setStatConfig(True)

    # for all non-normalised fit regions, we set to normalise by theory (TOM: what does this actually do??)
    if sample not in ["ttbar", "Z_jets", "singletop"]:
        print(f"For non-normalised regions, sample: {sample}, we set to normalise by theory")
        bkg_samples[sample].setNormByTheory()
        #bkg_samples[sample].addSystematic(Systematic(sample+"_NormSyst", configMgr.weights, 1.5, 0.5, "user", "userOverallSys"))

    if "Z_jets" in sample:
        # we define normalisations for Z+jets, singletop, ttbb, ttlight. Each normalisation constrained within single-bin control regions
        bkg_samples[sample].setNormFactor("mu_Z_jets", 1., 0., 5.)
        bkg_samples[sample].setNormRegions([("CR_Z", "cuts"), ("CR_4b", "cuts"), ("CR_3b_low_mTb", "cuts"), ("CR_3b_high_mTb", "cuts")])
    if "singletop" in sample:
        if doMonoShh:
            bkg_samples[sample].setNormFactor("mu_singletop", 1., 0., 5.)
            bkg_samples[sample].setNormRegions([("CR_Z", "cuts"), ("CR_4b", "cuts"), ("CR_3b_low_mTb", "cuts"), ("CR_3b_high_mTb", "cuts")])
        else:
            #bkg_samples[sample].setNormByTheory()
            bkg_samples[sample].addSystematic(Systematic(sample+"_NormSyst", configMgr.weights, 1.5, 0.5, "user", "userOverallSys"))
    # add all file inputs (should be three, I do logic check this before) mc16a,d,e:
    files = []
    files = [file_path for file_path in bkgFiles if sample in os.path.basename(file_path)]
    print(f"Adding files {files} for {sample}")
    bkg_samples[sample].addInputs(files)

    if len(files) == 3:
        print(f"Correct number of background files for process {sample}")
        #bkg_samples[sample].addInputs(files)
    else:
        raise ValueError(f"The number of files is more than it should be! Should be three, but it's {len(files)} for {sample}")

#--------------------
# List of systematics
#--------------------
# btagging example
btagging_weight_Eigen_B_0_down      = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_B_0__1down")
btagging_weight_Eigen_B_0_down      = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_B_0__1down")
btagging_weight_Eigen_B_0_up        = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_B_0__1up")
btagging_weight_Eigen_B_1_down      = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_B_1__1down")
btagging_weight_Eigen_B_1_up        = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_B_1__1up")
btagging_weight_Eigen_B_2_down      = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_B_2__1down")
btagging_weight_Eigen_B_2_up        = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_B_2__1up")
btagging_weight_Eigen_B_3_down      = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_B_3__1down")
btagging_weight_Eigen_B_3_up        = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_B_3__1up")
btagging_weight_Eigen_B_4_down      = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_B_4__1down")
btagging_weight_Eigen_B_4_up        = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_B_4__1up")
btagging_weight_Eigen_B_5_down      = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_B_5__1down")
btagging_weight_Eigen_B_5_up        = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_B_5__1up")
btagging_weight_Eigen_B_6_down      = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_B_6__1down")
btagging_weight_Eigen_B_6_up        = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_B_6__1up")
btagging_weight_Eigen_B_7_down      = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_B_7__1down")
btagging_weight_Eigen_B_7_up        = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_B_7__1up")
btagging_weight_Eigen_B_8_down      = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_B_8__1down")
btagging_weight_Eigen_B_8_up        = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_B_8__1up")

btagging_weight_Eigen_C_0_down      = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_C_0__1down")
btagging_weight_Eigen_C_0_up        = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_C_0__1up")
btagging_weight_Eigen_C_1_down      = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_C_1__1down")
btagging_weight_Eigen_C_1_up        = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_C_1__1up")
btagging_weight_Eigen_C_2_down      = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_C_2__1down")
btagging_weight_Eigen_C_2_up        = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_C_2__1up")
btagging_weight_Eigen_C_3_down      = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_C_3__1down")
btagging_weight_Eigen_C_3_up        = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_C_3__1up")

btagging_weight_Eigen_Light_0_down  = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_Light_0__1down")
btagging_weight_Eigen_Light_0_up    = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_Light_0__1up")
btagging_weight_Eigen_Light_1_down  = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_Light_1__1down")
btagging_weight_Eigen_Light_1_up    = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_Light_1__1up")
btagging_weight_Eigen_Light_2_down  = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_Light_2__1down")
btagging_weight_Eigen_Light_2_up    = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_Light_2__1up")
btagging_weight_Eigen_Light_3_down  = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_Light_3__1down")
btagging_weight_Eigen_Light_3_up    = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_Eigen_Light_3__1up")

btagging_weight_extrapol_down       = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_extrapolation__1down")
btagging_weight_extrapol_up         = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_extrapolation__1up")
btagging_weight_extrapol_charm_down = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_extrapolation_from_charm__1down")
btagging_weight_extrapol_charm_up   = replaceWeight(configMgr.weights, "weight_btag", "weight_btag_FT_EFF_extrapolation_from_charm__1up")
# JVT uncertainties
jvt_weight_down                     = replaceWeight(configMgr.weights, "weight_jvt", "weight_jvt_JET_JvtEfficiency__1down")
jvt_weight_up                       = replaceWeight(configMgr.weights, "weight_jvt", "weight_jvt_JET_JvtEfficiency__1up")

# MET trigger scale factors
met_trig_weight_down                = replaceWeight(configMgr.weights, "weight_met_trig_correct", "weight_met_trig_correct__down")
met_trig_weight_up                  = replaceWeight(configMgr.weights, "weight_met_trig_correct", "weight_met_trig_correct__up")

# PU uncertainties
pu_weight_down                      = replaceWeight(configMgr.weights, "weight_pu", "weight_pu_PRW_DATASF__1down")
pu_weight_up                        = replaceWeight(configMgr.weights, "weight_pu", "weight_pu_PRW_DATASF__1up")

# 7-point muR/muF variations (PMG recommendation).
weight_WZ_pmg_mur1_muf2=replaceWeight(configMgr.weights,"weight_mc","weight_pmg_MUR1__MUF2__PDF261000")
weight_WZ_pmg_mur1_muf05=replaceWeight(configMgr.weights,"weight_mc","weight_pmg_MUR1__MUF05__PDF261000")

weight_WZ_pmg_mur2_muf1=replaceWeight(configMgr.weights,"weight_mc","weight_pmg_MUR2__MUF1__PDF261000")
weight_WZ_pmg_mur05_muf1=replaceWeight(configMgr.weights,"weight_mc","weight_pmg_MUR05__MUF1__PDF261000")

weight_WZ_pmg_mur2_muf2=replaceWeight(configMgr.weights,"weight_mc","weight_pmg_MUR2__MUF2__PDF261000")
weight_WZ_pmg_mur05_muf05=replaceWeight(configMgr.weights,"weight_mc","weight_pmg_MUR05__MUF05__PDF261000")

weight_pmg_mur1_muf2 =  replaceWeight(configMgr.weights, "weight_mc", "weight_pmg_muR10__muF20")
weight_pmg_mur1_muf05 = replaceWeight(configMgr.weights, "weight_mc", "weight_pmg_muR10__muF05")

weight_pmg_mur2_muf1 =  replaceWeight(configMgr.weights, "weight_mc", "weight_pmg_muR20__muF10")
weight_pmg_mur05_muf1 = replaceWeight(configMgr.weights, "weight_mc", "weight_pmg_muR05__muF10")

weight_pmg_Var3cUp = replaceWeight(configMgr.weights, "weight_mc", "weight_pmg_Var3cUp")
weight_pmg_Var3cDown = replaceWeight(configMgr.weights, "weight_mc", "weight_pmg_Var3cDown")

systematics = {
  "bkgd": { # MC-only backgrounds, not normalised to data
    "default": { 
      # b-tagging (weight-based)
      "btagging_Eigen_B_0": Systematic("btagging_Eigen_B_0", configMgr.weights, btagging_weight_Eigen_B_0_up, btagging_weight_Eigen_B_0_down, "weight", "histoSys"),
      "btagging_Eigen_B_1": Systematic("btagging_Eigen_B_1", configMgr.weights, btagging_weight_Eigen_B_1_up, btagging_weight_Eigen_B_1_down, "weight", "histoSys"),
      "btagging_Eigen_B_2": Systematic("btagging_Eigen_B_2", configMgr.weights, btagging_weight_Eigen_B_2_up, btagging_weight_Eigen_B_2_down, "weight", "histoSys"),
      "btagging_Eigen_B_3": Systematic("btagging_Eigen_B_3", configMgr.weights, btagging_weight_Eigen_B_3_up, btagging_weight_Eigen_B_3_down, "weight", "histoSys"),
      "btagging_Eigen_B_4": Systematic("btagging_Eigen_B_4", configMgr.weights, btagging_weight_Eigen_B_4_up, btagging_weight_Eigen_B_4_down, "weight", "histoSys"),
      "btagging_Eigen_B_5": Systematic("btagging_Eigen_B_5", configMgr.weights, btagging_weight_Eigen_B_5_up, btagging_weight_Eigen_B_5_down, "weight", "histoSys"),
      "btagging_Eigen_B_6": Systematic("btagging_Eigen_B_6", configMgr.weights, btagging_weight_Eigen_B_6_up, btagging_weight_Eigen_B_6_down, "weight", "histoSys"),
      "btagging_Eigen_B_7": Systematic("btagging_Eigen_B_7", configMgr.weights, btagging_weight_Eigen_B_7_up, btagging_weight_Eigen_B_7_down, "weight", "histoSys"),
      "btagging_Eigen_B_8": Systematic("btagging_Eigen_B_8", configMgr.weights, btagging_weight_Eigen_B_8_up, btagging_weight_Eigen_B_8_down, "weight", "histoSys"),
      "btagging_Eigen_C_0": Systematic("btagging_Eigen_C_0", configMgr.weights, btagging_weight_Eigen_C_0_up, btagging_weight_Eigen_C_0_down, "weight", "histoSys"),
      "btagging_Eigen_C_1": Systematic("btagging_Eigen_C_1", configMgr.weights, btagging_weight_Eigen_C_1_up, btagging_weight_Eigen_C_1_down, "weight", "histoSys"),
      "btagging_Eigen_C_2": Systematic("btagging_Eigen_C_2", configMgr.weights, btagging_weight_Eigen_C_2_up, btagging_weight_Eigen_C_2_down, "weight", "histoSys"),
      "btagging_Eigen_C_3": Systematic("btagging_Eigen_C_3", configMgr.weights, btagging_weight_Eigen_C_3_up, btagging_weight_Eigen_C_3_down, "weight", "histoSys"),
      "btagging_Eigen_Light_0": Systematic("btagging_Eigen_Light_0", configMgr.weights, btagging_weight_Eigen_Light_0_up, btagging_weight_Eigen_Light_0_down, "weight", "histoSys"),
      "btagging_Eigen_Light_1": Systematic("btagging_Eigen_Light_1", configMgr.weights, btagging_weight_Eigen_Light_1_up, btagging_weight_Eigen_Light_1_down, "weight", "histoSys"),
      "btagging_Eigen_Light_2": Systematic("btagging_Eigen_Light_2", configMgr.weights, btagging_weight_Eigen_Light_2_up, btagging_weight_Eigen_Light_2_down, "weight", "histoSys"),
      "btagging_Eigen_Light_3": Systematic("btagging_Eigen_Light_3", configMgr.weights, btagging_weight_Eigen_Light_3_up, btagging_weight_Eigen_Light_3_down, "weight", "histoSys"),
      "btagging_extrapol": Systematic("bTag_extrapol",configMgr.weights,btagging_weight_extrapol_up,btagging_weight_extrapol_down,"weight","histoSys"),
      "btagging_extrapol_charm": Systematic("bTag_extrapol_charm",configMgr.weights,btagging_weight_extrapol_charm_up,btagging_weight_extrapol_charm_down,"weight","histoSys"),
      # JVT (weight-based)
      "jvt": Systematic("JVT", configMgr.weights, jvt_weight_up, jvt_weight_down, "weight", "histoSys"),
      # MET trigger
      "met_trig": Systematic("met_trigger", configMgr.weights, met_trig_weight_up, met_trig_weight_down, "weight", "histoSys")
    },
    "all": {
      # Met (tree-based)
      "met_ResoPara": Systematic("MET_ResoPara",configMgr.nomName,"_MET_SoftTrk_ResoPara",configMgr.nomName,"tree","histoSysOneSideSym"),
      "met_ResoPerp": Systematic("MET_ResoPerp",configMgr.nomName,"_MET_SoftTrk_ResoPerp",configMgr.nomName,"tree","histoSysOneSideSym"),
      "met_scale": Systematic("MET_scale",configMgr.nomName,"_MET_SoftTrk_ScaleUp","_MET_SoftTrk_ScaleDown","tree","histoSys"),
      # PRW systs
      "pileup_rew": Systematic("PRW_syst", configMgr.weights, pu_weight_up, pu_weight_down, "weight", "histoSys")
    }
  },
  "ttbar": {
    "default": { 
      # b-tagging (weight-based)
      "btagging_Eigen_B_0": Systematic("btagging_Eigen_B_0", configMgr.weights, btagging_weight_Eigen_B_0_up, btagging_weight_Eigen_B_0_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_B_1": Systematic("btagging_Eigen_B_1", configMgr.weights, btagging_weight_Eigen_B_1_up, btagging_weight_Eigen_B_1_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_B_2": Systematic("btagging_Eigen_B_2", configMgr.weights, btagging_weight_Eigen_B_2_up, btagging_weight_Eigen_B_2_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_B_3": Systematic("btagging_Eigen_B_3", configMgr.weights, btagging_weight_Eigen_B_3_up, btagging_weight_Eigen_B_3_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_B_4": Systematic("btagging_Eigen_B_4", configMgr.weights, btagging_weight_Eigen_B_4_up, btagging_weight_Eigen_B_4_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_B_5": Systematic("btagging_Eigen_B_5", configMgr.weights, btagging_weight_Eigen_B_5_up, btagging_weight_Eigen_B_5_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_B_6": Systematic("btagging_Eigen_B_6", configMgr.weights, btagging_weight_Eigen_B_6_up, btagging_weight_Eigen_B_6_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_B_7": Systematic("btagging_Eigen_B_7", configMgr.weights, btagging_weight_Eigen_B_7_up, btagging_weight_Eigen_B_7_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_B_8": Systematic("btagging_Eigen_B_8", configMgr.weights, btagging_weight_Eigen_B_8_up, btagging_weight_Eigen_B_8_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_C_0": Systematic("btagging_Eigen_C_0", configMgr.weights, btagging_weight_Eigen_C_0_up, btagging_weight_Eigen_C_0_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_C_1": Systematic("btagging_Eigen_C_1", configMgr.weights, btagging_weight_Eigen_C_1_up, btagging_weight_Eigen_C_1_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_C_2": Systematic("btagging_Eigen_C_2", configMgr.weights, btagging_weight_Eigen_C_2_up, btagging_weight_Eigen_C_2_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_C_3": Systematic("btagging_Eigen_C_3", configMgr.weights, btagging_weight_Eigen_C_3_up, btagging_weight_Eigen_C_3_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_Light_0": Systematic("btagging_Eigen_Light_0", configMgr.weights, btagging_weight_Eigen_Light_0_up, btagging_weight_Eigen_Light_0_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_Light_1": Systematic("btagging_Eigen_Light_1", configMgr.weights, btagging_weight_Eigen_Light_1_up, btagging_weight_Eigen_Light_1_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_Light_2": Systematic("btagging_Eigen_Light_2", configMgr.weights, btagging_weight_Eigen_Light_2_up, btagging_weight_Eigen_Light_2_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_Light_3": Systematic("btagging_Eigen_Light_3", configMgr.weights, btagging_weight_Eigen_Light_3_up, btagging_weight_Eigen_Light_3_down, "weight", "overallNormHistoSys"),
      "btagging_extrapol": Systematic("bTag_extrapol",configMgr.weights,btagging_weight_extrapol_up,btagging_weight_extrapol_down,"weight","overallNormHistoSys"),
      "btagging_extrapol_charm": Systematic("bTag_extrapol_charm",configMgr.weights,btagging_weight_extrapol_charm_up,btagging_weight_extrapol_charm_down,"weight","overallNormHistoSys"),
      # JVT (weight-based)
      "jvt": Systematic("JVT", configMgr.weights, jvt_weight_up, jvt_weight_down, "weight", "overallNormHistoSys"),
      # MET trigger
      "met_trig": Systematic("met_trigger", configMgr.weights, met_trig_weight_up, met_trig_weight_down, "weight", "overallNormHistoSys")
    },
    "all": {
      # Met (tree-based)
      "met_ResoPara": Systematic("MET_ResoPara",configMgr.nomName,"_MET_SoftTrk_ResoPara",configMgr.nomName,"tree","overallNormHistoSysOneSideSym"),
      "met_ResoPerp": Systematic("MET_ResoPerp",configMgr.nomName,"_MET_SoftTrk_ResoPerp",configMgr.nomName,"tree","overallNormHistoSysOneSideSym"),
      "met_scale": Systematic("MET_scale",configMgr.nomName,"_MET_SoftTrk_ScaleUp","_MET_SoftTrk_ScaleDown","tree","overallNormHistoSys"),
      # PRW systs
      "pileup_rew": Systematic("PRW_syst", configMgr.weights, pu_weight_up, pu_weight_down, "weight", "overallNormHistoSys")
    },
    "theo":{
      "muR_syst": Systematic("ttbar_muR_syst", configMgr.weights, weight_pmg_mur2_muf1, weight_pmg_mur05_muf1, "weight", "histoSys"),
      "muF_syst": Systematic("ttbar_muF_syst", configMgr.weights, weight_pmg_mur1_muf2, weight_pmg_mur1_muf05, "weight", "histoSys"),
      "Var3c_syst": Systematic("ttbar_Var3c_syst", configMgr.weights, weight_pmg_Var3cUp, weight_pmg_Var3cDown, "weight", "histoSys")
    }
  },
  "ttcc": {
    "default": { 
      # b-tagging (weight-based)
      "btagging_Eigen_B_0": Systematic("btagging_Eigen_B_0", configMgr.weights, btagging_weight_Eigen_B_0_up, btagging_weight_Eigen_B_0_down, "weight", "histoSys"),
      "btagging_Eigen_B_1": Systematic("btagging_Eigen_B_1", configMgr.weights, btagging_weight_Eigen_B_1_up, btagging_weight_Eigen_B_1_down, "weight", "histoSys"),
      "btagging_Eigen_B_2": Systematic("btagging_Eigen_B_2", configMgr.weights, btagging_weight_Eigen_B_2_up, btagging_weight_Eigen_B_2_down, "weight", "histoSys"),
      "btagging_Eigen_B_3": Systematic("btagging_Eigen_B_3", configMgr.weights, btagging_weight_Eigen_B_3_up, btagging_weight_Eigen_B_3_down, "weight", "histoSys"),
      "btagging_Eigen_B_4": Systematic("btagging_Eigen_B_4", configMgr.weights, btagging_weight_Eigen_B_4_up, btagging_weight_Eigen_B_4_down, "weight", "histoSys"),
      "btagging_Eigen_B_5": Systematic("btagging_Eigen_B_5", configMgr.weights, btagging_weight_Eigen_B_5_up, btagging_weight_Eigen_B_5_down, "weight", "histoSys"),
      "btagging_Eigen_B_6": Systematic("btagging_Eigen_B_6", configMgr.weights, btagging_weight_Eigen_B_6_up, btagging_weight_Eigen_B_6_down, "weight", "histoSys"),
      "btagging_Eigen_B_7": Systematic("btagging_Eigen_B_7", configMgr.weights, btagging_weight_Eigen_B_7_up, btagging_weight_Eigen_B_7_down, "weight", "histoSys"),
      "btagging_Eigen_B_8": Systematic("btagging_Eigen_B_8", configMgr.weights, btagging_weight_Eigen_B_8_up, btagging_weight_Eigen_B_8_down, "weight", "histoSys"),
      "btagging_Eigen_C_0": Systematic("btagging_Eigen_C_0", configMgr.weights, btagging_weight_Eigen_C_0_up, btagging_weight_Eigen_C_0_down, "weight", "histoSys"),
      "btagging_Eigen_C_1": Systematic("btagging_Eigen_C_1", configMgr.weights, btagging_weight_Eigen_C_1_up, btagging_weight_Eigen_C_1_down, "weight", "histoSys"),
      "btagging_Eigen_C_2": Systematic("btagging_Eigen_C_2", configMgr.weights, btagging_weight_Eigen_C_2_up, btagging_weight_Eigen_C_2_down, "weight", "histoSys"),
      "btagging_Eigen_C_3": Systematic("btagging_Eigen_C_3", configMgr.weights, btagging_weight_Eigen_C_3_up, btagging_weight_Eigen_C_3_down, "weight", "histoSys"),
      "btagging_Eigen_Light_0": Systematic("btagging_Eigen_Light_0", configMgr.weights, btagging_weight_Eigen_Light_0_up, btagging_weight_Eigen_Light_0_down, "weight", "histoSys"),
      "btagging_Eigen_Light_1": Systematic("btagging_Eigen_Light_1", configMgr.weights, btagging_weight_Eigen_Light_1_up, btagging_weight_Eigen_Light_1_down, "weight", "histoSys"),
      "btagging_Eigen_Light_2": Systematic("btagging_Eigen_Light_2", configMgr.weights, btagging_weight_Eigen_Light_2_up, btagging_weight_Eigen_Light_2_down, "weight", "histoSys"),
      "btagging_Eigen_Light_3": Systematic("btagging_Eigen_Light_3", configMgr.weights, btagging_weight_Eigen_Light_3_up, btagging_weight_Eigen_Light_3_down, "weight", "histoSys"),
      "btagging_extrapol": Systematic("bTag_extrapol",configMgr.weights,btagging_weight_extrapol_up,btagging_weight_extrapol_down,"weight","histoSys"),
      "btagging_extrapol_charm": Systematic("bTag_extrapol_charm",configMgr.weights,btagging_weight_extrapol_charm_up,btagging_weight_extrapol_charm_down,"weight","histoSys"),
      # JVT (weight-based)
      "jvt": Systematic("JVT", configMgr.weights, jvt_weight_up, jvt_weight_down, "weight", "histoSys"),
      # MET trigger
      "met_trig": Systematic("met_trigger", configMgr.weights, met_trig_weight_up, met_trig_weight_down, "weight", "histoSys")
    },
    "all": {
      # Met (tree-based)
      "met_ResoPara": Systematic("MET_ResoPara",configMgr.nomName,"_MET_SoftTrk_ResoPara",configMgr.nomName,"tree","histoSysOneSideSym"),
      "met_ResoPerp": Systematic("MET_ResoPerp",configMgr.nomName,"_MET_SoftTrk_ResoPerp",configMgr.nomName,"tree","histoSysOneSideSym"),
      "met_scale": Systematic("MET_scale",configMgr.nomName,"_MET_SoftTrk_ScaleUp","_MET_SoftTrk_ScaleDown","tree","histoSys"),
      # PRW systs
      "pileup_rew": Systematic("PRW_syst", configMgr.weights, pu_weight_up, pu_weight_down, "weight", "histoSys")
    },
    "theo":{
      "muR_syst": Systematic("ttcc_muR_syst", configMgr.weights, weight_pmg_mur2_muf1, weight_pmg_mur05_muf1, "weight", "histoSys"),
      "muF_syst": Systematic("ttcc_muF_syst", configMgr.weights, weight_pmg_mur1_muf2, weight_pmg_mur1_muf05, "weight", "histoSys"),
      "Var3c_syst": Systematic("ttcc_Var3c_syst", configMgr.weights, weight_pmg_Var3cUp, weight_pmg_Var3cDown, "weight", "histoSys")
    }
  },

  "W_jets": {
    "theo": {
      "WZ_muR_syst": Systematic("WZ_muR_syst",configMgr.weights,weight_WZ_pmg_mur2_muf1,weight_WZ_pmg_mur05_muf1,"weight", "histoSys"),
      "WZ_muF_syst": Systematic("WZ_muF_syst",configMgr.weights,weight_WZ_pmg_mur1_muf2,weight_WZ_pmg_mur1_muf05,"weight", "histoSys"),
      "WZ_muR_muF_syst": Systematic("WZ_muR_muF_syst",configMgr.weights,weight_WZ_pmg_mur2_muf2,weight_WZ_pmg_mur05_muf05, "weight", "histoSys")
      }
  },

  "singletop": {
    "default": { 
      # b-tagging (weight-based)
      "btagging_Eigen_B_0": Systematic("btagging_Eigen_B_0", configMgr.weights, btagging_weight_Eigen_B_0_up, btagging_weight_Eigen_B_0_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_B_1": Systematic("btagging_Eigen_B_1", configMgr.weights, btagging_weight_Eigen_B_1_up, btagging_weight_Eigen_B_1_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_B_2": Systematic("btagging_Eigen_B_2", configMgr.weights, btagging_weight_Eigen_B_2_up, btagging_weight_Eigen_B_2_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_B_3": Systematic("btagging_Eigen_B_3", configMgr.weights, btagging_weight_Eigen_B_3_up, btagging_weight_Eigen_B_3_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_B_4": Systematic("btagging_Eigen_B_4", configMgr.weights, btagging_weight_Eigen_B_4_up, btagging_weight_Eigen_B_4_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_B_5": Systematic("btagging_Eigen_B_5", configMgr.weights, btagging_weight_Eigen_B_5_up, btagging_weight_Eigen_B_5_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_B_6": Systematic("btagging_Eigen_B_6", configMgr.weights, btagging_weight_Eigen_B_6_up, btagging_weight_Eigen_B_6_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_B_7": Systematic("btagging_Eigen_B_7", configMgr.weights, btagging_weight_Eigen_B_7_up, btagging_weight_Eigen_B_7_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_B_8": Systematic("btagging_Eigen_B_8", configMgr.weights, btagging_weight_Eigen_B_8_up, btagging_weight_Eigen_B_8_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_C_0": Systematic("btagging_Eigen_C_0", configMgr.weights, btagging_weight_Eigen_C_0_up, btagging_weight_Eigen_C_0_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_C_1": Systematic("btagging_Eigen_C_1", configMgr.weights, btagging_weight_Eigen_C_1_up, btagging_weight_Eigen_C_1_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_C_2": Systematic("btagging_Eigen_C_2", configMgr.weights, btagging_weight_Eigen_C_2_up, btagging_weight_Eigen_C_2_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_C_3": Systematic("btagging_Eigen_C_3", configMgr.weights, btagging_weight_Eigen_C_3_up, btagging_weight_Eigen_C_3_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_Light_0": Systematic("btagging_Eigen_Light_0", configMgr.weights, btagging_weight_Eigen_Light_0_up, btagging_weight_Eigen_Light_0_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_Light_1": Systematic("btagging_Eigen_Light_1", configMgr.weights, btagging_weight_Eigen_Light_1_up, btagging_weight_Eigen_Light_1_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_Light_2": Systematic("btagging_Eigen_Light_2", configMgr.weights, btagging_weight_Eigen_Light_2_up, btagging_weight_Eigen_Light_2_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_Light_3": Systematic("btagging_Eigen_Light_3", configMgr.weights, btagging_weight_Eigen_Light_3_up, btagging_weight_Eigen_Light_3_down, "weight", "overallNormHistoSys"),
      "btagging_extrapol": Systematic("bTag_extrapol",configMgr.weights,btagging_weight_extrapol_up,btagging_weight_extrapol_down,"weight","overallNormHistoSys"),
      "btagging_extrapol_charm": Systematic("bTag_extrapol_charm",configMgr.weights,btagging_weight_extrapol_charm_up,btagging_weight_extrapol_charm_down,"weight","overallNormHistoSys"),
      # JVT (weight-based)
      "jvt": Systematic("JVT", configMgr.weights, jvt_weight_up, jvt_weight_down, "weight", "overallNormHistoSys"),
      # MET trigger
      "met_trig": Systematic("met_trigger", configMgr.weights, met_trig_weight_up, met_trig_weight_down, "weight", "overallNormHistoSys")
    },
    "all": {
      # Met (tree-based)
      "met_ResoPara": Systematic("MET_ResoPara",configMgr.nomName,"_MET_SoftTrk_ResoPara",configMgr.nomName,"tree","overallNormHistoSysOneSideSym"),
      "met_ResoPerp": Systematic("MET_ResoPerp",configMgr.nomName,"_MET_SoftTrk_ResoPerp",configMgr.nomName,"tree","overallNormHistoSysOneSideSym"),
      "met_scale": Systematic("MET_scale",configMgr.nomName,"_MET_SoftTrk_ScaleUp","_MET_SoftTrk_ScaleDown","tree","overallNormHistoSys"),
      # PRW systs
      "pileup_rew": Systematic("PRW_syst", configMgr.weights, pu_weight_up, pu_weight_down, "weight", "overallNormHistoSys")
    },
    "theo": {
      "muR_syst": Systematic("singletop_muR_syst", configMgr.weights, weight_pmg_mur2_muf1, weight_pmg_mur05_muf1, "weight", "histoSys"),
      "muF_syst": Systematic("singletop_muF_syst", configMgr.weights, weight_pmg_mur1_muf2, weight_pmg_mur1_muf05, "weight", "histoSys"),
      "Var3c_syst": Systematic("singletop_Var3c_syst", configMgr.weights, weight_pmg_Var3cUp, weight_pmg_Var3cDown, "weight", "histoSys")
    }
  },
  "Z_jets": {
    "default": { 
      # b-tagging (weight-based)
      "btagging_Eigen_B_0": Systematic("btagging_Eigen_B_0", configMgr.weights, btagging_weight_Eigen_B_0_up, btagging_weight_Eigen_B_0_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_B_1": Systematic("btagging_Eigen_B_1", configMgr.weights, btagging_weight_Eigen_B_1_up, btagging_weight_Eigen_B_1_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_B_2": Systematic("btagging_Eigen_B_2", configMgr.weights, btagging_weight_Eigen_B_2_up, btagging_weight_Eigen_B_2_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_B_3": Systematic("btagging_Eigen_B_3", configMgr.weights, btagging_weight_Eigen_B_3_up, btagging_weight_Eigen_B_3_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_B_4": Systematic("btagging_Eigen_B_4", configMgr.weights, btagging_weight_Eigen_B_4_up, btagging_weight_Eigen_B_4_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_B_5": Systematic("btagging_Eigen_B_5", configMgr.weights, btagging_weight_Eigen_B_5_up, btagging_weight_Eigen_B_5_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_B_6": Systematic("btagging_Eigen_B_6", configMgr.weights, btagging_weight_Eigen_B_6_up, btagging_weight_Eigen_B_6_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_B_7": Systematic("btagging_Eigen_B_7", configMgr.weights, btagging_weight_Eigen_B_7_up, btagging_weight_Eigen_B_7_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_B_8": Systematic("btagging_Eigen_B_8", configMgr.weights, btagging_weight_Eigen_B_8_up, btagging_weight_Eigen_B_8_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_C_0": Systematic("btagging_Eigen_C_0", configMgr.weights, btagging_weight_Eigen_C_0_up, btagging_weight_Eigen_C_0_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_C_1": Systematic("btagging_Eigen_C_1", configMgr.weights, btagging_weight_Eigen_C_1_up, btagging_weight_Eigen_C_1_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_C_2": Systematic("btagging_Eigen_C_2", configMgr.weights, btagging_weight_Eigen_C_2_up, btagging_weight_Eigen_C_2_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_C_3": Systematic("btagging_Eigen_C_3", configMgr.weights, btagging_weight_Eigen_C_3_up, btagging_weight_Eigen_C_3_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_Light_0": Systematic("btagging_Eigen_Light_0", configMgr.weights, btagging_weight_Eigen_Light_0_up, btagging_weight_Eigen_Light_0_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_Light_1": Systematic("btagging_Eigen_Light_1", configMgr.weights, btagging_weight_Eigen_Light_1_up, btagging_weight_Eigen_Light_1_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_Light_2": Systematic("btagging_Eigen_Light_2", configMgr.weights, btagging_weight_Eigen_Light_2_up, btagging_weight_Eigen_Light_2_down, "weight", "overallNormHistoSys"),
      "btagging_Eigen_Light_3": Systematic("btagging_Eigen_Light_3", configMgr.weights, btagging_weight_Eigen_Light_3_up, btagging_weight_Eigen_Light_3_down, "weight", "overallNormHistoSys"),
      "btagging_extrapol": Systematic("bTag_extrapol",configMgr.weights,btagging_weight_extrapol_up,btagging_weight_extrapol_down,"weight","overallNormHistoSys"),
      "btagging_extrapol_charm": Systematic("bTag_extrapol_charm",configMgr.weights,btagging_weight_extrapol_charm_up,btagging_weight_extrapol_charm_down,"weight","overallNormHistoSys"),
      # JVT (weight-based)
      "jvt": Systematic("JVT", configMgr.weights, jvt_weight_up, jvt_weight_down, "weight", "overallNormHistoSys"),
      # MET trigger
      "met_trig": Systematic("met_trigger", configMgr.weights, met_trig_weight_up, met_trig_weight_down, "weight", "overallNormHistoSys")
    },
    "all": {
      # Met (tree-based)
      "met_ResoPara": Systematic("MET_ResoPara",configMgr.nomName,"_MET_SoftTrk_ResoPara",configMgr.nomName,"tree","overallNormHistoSysOneSideSym"),
      "met_ResoPerp": Systematic("MET_ResoPerp",configMgr.nomName,"_MET_SoftTrk_ResoPerp",configMgr.nomName,"tree","overallNormHistoSysOneSideSym"),
      "met_scale": Systematic("MET_scale",configMgr.nomName,"_MET_SoftTrk_ScaleUp","_MET_SoftTrk_ScaleDown","tree","overallNormHistoSys"),
      # PRW systs
      "pileup_rew": Systematic("PRW_syst", configMgr.weights, pu_weight_up, pu_weight_down, "weight", "overallNormHistoSys")
    },
    "theo": {
      "WZ_muR_syst": Systematic("WZ_muR_syst",configMgr.weights,weight_WZ_pmg_mur2_muf1,weight_WZ_pmg_mur05_muf1,"weight", "histoSys"),
      "WZ_muF_syst": Systematic("WZ_muF_syst",configMgr.weights,weight_WZ_pmg_mur1_muf2,weight_WZ_pmg_mur1_muf05,"weight", "histoSys"),
      "WZ_muR_muF_syst": Systematic("WZ_muR_muF_syst",configMgr.weights,weight_WZ_pmg_mur2_muf2,weight_WZ_pmg_mur05_muf05, "weight", "histoSys")
    }
  },
  "signal": {
    "default": { 
      # b-tagging (weight-based)
      "btagging_Eigen_B_0": Systematic("btagging_Eigen_B_0", configMgr.weights, btagging_weight_Eigen_B_0_up, btagging_weight_Eigen_B_0_down, "weight", "histoSys"),
      "btagging_Eigen_B_1": Systematic("btagging_Eigen_B_1", configMgr.weights, btagging_weight_Eigen_B_1_up, btagging_weight_Eigen_B_1_down, "weight", "histoSys"),
      "btagging_Eigen_B_2": Systematic("btagging_Eigen_B_2", configMgr.weights, btagging_weight_Eigen_B_2_up, btagging_weight_Eigen_B_2_down, "weight", "histoSys"),
      "btagging_Eigen_B_3": Systematic("btagging_Eigen_B_3", configMgr.weights, btagging_weight_Eigen_B_3_up, btagging_weight_Eigen_B_3_down, "weight", "histoSys"),
      "btagging_Eigen_B_4": Systematic("btagging_Eigen_B_4", configMgr.weights, btagging_weight_Eigen_B_4_up, btagging_weight_Eigen_B_4_down, "weight", "histoSys"),
      "btagging_Eigen_B_5": Systematic("btagging_Eigen_B_5", configMgr.weights, btagging_weight_Eigen_B_5_up, btagging_weight_Eigen_B_5_down, "weight", "histoSys"),
      "btagging_Eigen_B_6": Systematic("btagging_Eigen_B_6", configMgr.weights, btagging_weight_Eigen_B_6_up, btagging_weight_Eigen_B_6_down, "weight", "histoSys"),
      "btagging_Eigen_B_7": Systematic("btagging_Eigen_B_7", configMgr.weights, btagging_weight_Eigen_B_7_up, btagging_weight_Eigen_B_7_down, "weight", "histoSys"),
      "btagging_Eigen_B_8": Systematic("btagging_Eigen_B_8", configMgr.weights, btagging_weight_Eigen_B_8_up, btagging_weight_Eigen_B_8_down, "weight", "histoSys"),
      "btagging_Eigen_C_0": Systematic("btagging_Eigen_C_0", configMgr.weights, btagging_weight_Eigen_C_0_up, btagging_weight_Eigen_C_0_down, "weight", "histoSys"),
      "btagging_Eigen_C_1": Systematic("btagging_Eigen_C_1", configMgr.weights, btagging_weight_Eigen_C_1_up, btagging_weight_Eigen_C_1_down, "weight", "histoSys"),
      "btagging_Eigen_C_2": Systematic("btagging_Eigen_C_2", configMgr.weights, btagging_weight_Eigen_C_2_up, btagging_weight_Eigen_C_2_down, "weight", "histoSys"),
      "btagging_Eigen_C_3": Systematic("btagging_Eigen_C_3", configMgr.weights, btagging_weight_Eigen_C_3_up, btagging_weight_Eigen_C_3_down, "weight", "histoSys"),
      "btagging_Eigen_Light_0": Systematic("btagging_Eigen_Light_0", configMgr.weights, btagging_weight_Eigen_Light_0_up, btagging_weight_Eigen_Light_0_down, "weight", "histoSys"),
      "btagging_Eigen_Light_1": Systematic("btagging_Eigen_Light_1", configMgr.weights, btagging_weight_Eigen_Light_1_up, btagging_weight_Eigen_Light_1_down, "weight", "histoSys"),
      "btagging_Eigen_Light_2": Systematic("btagging_Eigen_Light_2", configMgr.weights, btagging_weight_Eigen_Light_2_up, btagging_weight_Eigen_Light_2_down, "weight", "histoSys"),
      "btagging_Eigen_Light_3": Systematic("btagging_Eigen_Light_3", configMgr.weights, btagging_weight_Eigen_Light_3_up, btagging_weight_Eigen_Light_3_down, "weight", "histoSys"),
      "btagging_extrapol": Systematic("bTag_extrapol",configMgr.weights,btagging_weight_extrapol_up,btagging_weight_extrapol_down,"weight","histoSys"),
      "btagging_extrapol_charm": Systematic("bTag_extrapol_charm",configMgr.weights,btagging_weight_extrapol_charm_up,btagging_weight_extrapol_charm_down,"weight","histoSys"),
      # JVT (weight-based)
      "jvt": Systematic("JVT", configMgr.weights, jvt_weight_up, jvt_weight_down, "weight", "histoSys"),
      # MET trigger
      "met_trig": Systematic("met_trigger", configMgr.weights, met_trig_weight_up, met_trig_weight_down, "weight", "histoSys")
    },
    "all": {
      # Met (tree-based)
      "met_ResoPara": Systematic("MET_ResoPara",configMgr.nomName,"_MET_SoftTrk_ResoPara",configMgr.nomName,"tree","histoSysOneSideSym"),
      "met_ResoPerp": Systematic("MET_ResoPerp",configMgr.nomName,"_MET_SoftTrk_ResoPerp",configMgr.nomName,"tree","histoSysOneSideSym"),
      "met_scale": Systematic("MET_scale",configMgr.nomName,"_MET_SoftTrk_ScaleUp","_MET_SoftTrk_ScaleDown","tree","histoSys"),
      # PRW systs
      "pileup_rew": Systematic("PRW_syst", configMgr.weights, pu_weight_up, pu_weight_down, "weight", "histoSys")
    }
  }
}


if doMonoShh:
    print("Keeping normalised systematic types for singletop")
else:
    systematics['singletop'] = {
        "default": { 
            # b-tagging (weight-based)
            "btagging_Eigen_B_0": Systematic("btagging_Eigen_B_0", configMgr.weights, btagging_weight_Eigen_B_0_up, btagging_weight_Eigen_B_0_down, "weight", "overallHistoSys"),
            "btagging_Eigen_B_1": Systematic("btagging_Eigen_B_1", configMgr.weights, btagging_weight_Eigen_B_1_up, btagging_weight_Eigen_B_1_down, "weight", "overallHistoSys"),
            "btagging_Eigen_B_2": Systematic("btagging_Eigen_B_2", configMgr.weights, btagging_weight_Eigen_B_2_up, btagging_weight_Eigen_B_2_down, "weight", "overallHistoSys"),
            "btagging_Eigen_B_3": Systematic("btagging_Eigen_B_3", configMgr.weights, btagging_weight_Eigen_B_3_up, btagging_weight_Eigen_B_3_down, "weight", "overallHistoSys"),
            "btagging_Eigen_B_4": Systematic("btagging_Eigen_B_4", configMgr.weights, btagging_weight_Eigen_B_4_up, btagging_weight_Eigen_B_4_down, "weight", "overallHistoSys"),
            "btagging_Eigen_B_5": Systematic("btagging_Eigen_B_5", configMgr.weights, btagging_weight_Eigen_B_5_up, btagging_weight_Eigen_B_5_down, "weight", "overallHistoSys"),
            "btagging_Eigen_B_6": Systematic("btagging_Eigen_B_6", configMgr.weights, btagging_weight_Eigen_B_6_up, btagging_weight_Eigen_B_6_down, "weight", "overallHistoSys"),
            "btagging_Eigen_B_7": Systematic("btagging_Eigen_B_7", configMgr.weights, btagging_weight_Eigen_B_7_up, btagging_weight_Eigen_B_7_down, "weight", "overallHistoSys"),
            "btagging_Eigen_B_8": Systematic("btagging_Eigen_B_8", configMgr.weights, btagging_weight_Eigen_B_8_up, btagging_weight_Eigen_B_8_down, "weight", "overallHistoSys"),
            "btagging_Eigen_C_0": Systematic("btagging_Eigen_C_0", configMgr.weights, btagging_weight_Eigen_C_0_up, btagging_weight_Eigen_C_0_down, "weight", "overallHistoSys"),
            "btagging_Eigen_C_1": Systematic("btagging_Eigen_C_1", configMgr.weights, btagging_weight_Eigen_C_1_up, btagging_weight_Eigen_C_1_down, "weight", "overallHistoSys"),
            "btagging_Eigen_C_2": Systematic("btagging_Eigen_C_2", configMgr.weights, btagging_weight_Eigen_C_2_up, btagging_weight_Eigen_C_2_down, "weight", "overallHistoSys"),
            "btagging_Eigen_C_3": Systematic("btagging_Eigen_C_3", configMgr.weights, btagging_weight_Eigen_C_3_up, btagging_weight_Eigen_C_3_down, "weight", "overallHistoSys"),
            "btagging_Eigen_Light_0": Systematic("btagging_Eigen_Light_0", configMgr.weights, btagging_weight_Eigen_Light_0_up, btagging_weight_Eigen_Light_0_down, "weight", "overallHistoSys"),
            "btagging_Eigen_Light_1": Systematic("btagging_Eigen_Light_1", configMgr.weights, btagging_weight_Eigen_Light_1_up, btagging_weight_Eigen_Light_1_down, "weight", "overallHistoSys"),
            "btagging_Eigen_Light_2": Systematic("btagging_Eigen_Light_2", configMgr.weights, btagging_weight_Eigen_Light_2_up, btagging_weight_Eigen_Light_2_down, "weight", "overallHistoSys"),
            "btagging_Eigen_Light_3": Systematic("btagging_Eigen_Light_3", configMgr.weights, btagging_weight_Eigen_Light_3_up, btagging_weight_Eigen_Light_3_down, "weight", "overallHistoSys"),
            "btagging_extrapol": Systematic("bTag_extrapol",configMgr.weights,btagging_weight_extrapol_up,btagging_weight_extrapol_down,"weight","overallHistoSys"),
            "btagging_extrapol_charm": Systematic("bTag_extrapol_charm",configMgr.weights,btagging_weight_extrapol_charm_up,btagging_weight_extrapol_charm_down,"weight","overallHistoSys"),
            # JVT (weight-based)
            "jvt": Systematic("JVT", configMgr.weights, jvt_weight_up, jvt_weight_down, "weight", "overallHistoSys"),
            # MET trigger
            "met_trig": Systematic("met_trigger", configMgr.weights, met_trig_weight_up, met_trig_weight_down, "weight", "overallHistoSys")
        },
        "all": {
            # Met (tree-based)
            "met_ResoPara": Systematic("MET_ResoPara",configMgr.nomName,"_MET_SoftTrk_ResoPara",configMgr.nomName,"tree","histoSysOneSideSym"),
            "met_ResoPerp": Systematic("MET_ResoPerp",configMgr.nomName,"_MET_SoftTrk_ResoPerp",configMgr.nomName,"tree","histoSysOneSideSym"),
            "met_scale": Systematic("MET_scale",configMgr.nomName,"_MET_SoftTrk_ScaleUp","_MET_SoftTrk_ScaleDown","tree","histoSys"),
            # PRW systs
            "pileup_rew": Systematic("PRW_syst", configMgr.weights, pu_weight_up, pu_weight_down, "weight", "overallHistoSys")
        },
        "theo": {
            "muR_syst": Systematic("singletop_muR_syst", configMgr.weights, weight_pmg_mur2_muf1, weight_pmg_mur05_muf1, "weight", "histoSys"),
            "muF_syst": Systematic("singletop_muF_syst", configMgr.weights, weight_pmg_mur1_muf2, weight_pmg_mur1_muf05, "weight", "histoSys"),
            "Var3c_syst": Systematic("singletop_Var3c_syst", configMgr.weights, weight_pmg_Var3cUp, weight_pmg_Var3cDown, "weight", "histoSys")
        }
    }


# JER systematics
JER_systs = [
  'JET_JER_EffectiveNP_1',
  'JET_JER_EffectiveNP_2',
  'JET_JER_EffectiveNP_3',
  'JET_JER_EffectiveNP_4',
  'JET_JER_EffectiveNP_5',
  'JET_JER_EffectiveNP_6',
  'JET_JER_EffectiveNP_7',
  'JET_JER_EffectiveNP_8',
  'JET_JER_EffectiveNP_9',
  'JET_JER_EffectiveNP_10',
  'JET_JER_EffectiveNP_11',
  'JET_JER_EffectiveNP_12restTerm',
  'JET_JER_DataVsMC_MC16'
]

# JMS systematics
JMS_systs = [
  'JET_Rtrk_Baseline_frozen_mass',
  'JET_Rtrk_Baseline_mass',
  'JET_Rtrk_Baseline_pT',
  'JET_Rtrk_Closure_mass',
  'JET_Rtrk_Closure_pT',
  'JET_Rtrk_ExtraComp_Baseline_frozen_mass',
  'JET_Rtrk_ExtraComp_Modelling_frozen_mass',
  'JET_Rtrk_Modelling_frozen_mass',
  'JET_Rtrk_Modelling_mass',
  'JET_Rtrk_Modelling_pT',
  'JET_Rtrk_TotalStat_frozen_mass',
  'JET_Rtrk_TotalStat_mass',
  'JET_Rtrk_TotalStat_pT',
  'JET_Rtrk_Tracking1_frozen_mass',
  'JET_Rtrk_Tracking2_frozen_mass',
  'JET_Rtrk_Tracking3_frozen_mass',
  'JET_Rtrk_Tracking_mass',
  'JET_Rtrk_Tracking_pT'
]

# JES systematics
JES_systs = [
  'JET_BJES_Response',
  'JET_EffectiveNP_Detector1',
  'JET_EffectiveNP_Detector2',
  'JET_EffectiveNP_Mixed1',
  'JET_EffectiveNP_Mixed2',
  'JET_EffectiveNP_Mixed3',
  'JET_EffectiveNP_Modelling1',
  'JET_EffectiveNP_Modelling2',
  'JET_EffectiveNP_Modelling3',
  'JET_EffectiveNP_Modelling4',
  'JET_EffectiveNP_R10_1',
  'JET_EffectiveNP_R10_2',
  'JET_EffectiveNP_R10_3',
  'JET_EffectiveNP_R10_4',
  'JET_EffectiveNP_R10_5',
  'JET_EffectiveNP_R10_6restTerm',
  'JET_EffectiveNP_Statistical1',
  'JET_EffectiveNP_Statistical2',
  'JET_EffectiveNP_Statistical3',
  'JET_EffectiveNP_Statistical4',
  'JET_EffectiveNP_Statistical5',
  'JET_EffectiveNP_Statistical6',
  'JET_EtaIntercalibration_Modelling',
  'JET_EtaIntercalibration_NonClosure_2018data',
  'JET_EtaIntercalibration_NonClosure_highE',
  'JET_EtaIntercalibration_NonClosure_posEta',
  'JET_EtaIntercalibration_NonClosure_negEta',
  'JET_EtaIntercalibration_R10_TotalStat',
  'JET_EtaIntercalibration_TotalStat',
  'JET_Flavor_Composition',
  'JET_Flavor_Response'
]

if doExp_syst:
    print("Running systematics")
    runSystematics = True
else:
    print("No systematics")
    runSystematics = False


def add_experimental_systematics(channels):
    global systematics
    if not runSystematics: return
    JER_syst_values = {}
    JMS_syst_values = {}
    JES_syst_values = {}
    for channel in channels:

        channel_name = channel.name.replace('cuts_', '').replace("m_hh_new_min_dR",'').replace("_SR","SR")
        #if "VR" in channel_name: continue

        # for json specific naming, we use _monoShh here
        if "CR" in channel_name or "VR" in channel_name:
            channel_name = channel_name + "_monoShh"

        #channel_name += "_monoShh"
        JER_syst_values[channel_name] = {}
        JMS_syst_values[channel_name] = {}
        JES_syst_values[channel_name] = {}
        print(f"TOM: what channels are we applying systematics for? {channel.name}")
        print(f"samples within channel {channel.name} {channel.sampleList}")
        # loop over each sample
        for sample in channel.sampleList:
            sample_name = sample.name.replace("", "")
            # just check what's actually in channel.sampleList (should be all samples in CR for example)
            print("TOM: what samples are in channel.sampleList? ", channel.sampleList)
            # do not add systematics to data
            if 'data' in sample_name: continue

            # have to treat ttbar differently (same sample split in three)
            if 'tt' in sample_name:
                JER_syst_values[channel_name]["ttbar_sys"] = {}
                JMS_syst_values[channel_name]["ttbar_sys"] = {}
                JES_syst_values[channel_name]["ttbar_sys"] = {}
            else:
                JER_syst_values[channel_name][sample_name] = {}
                JMS_syst_values[channel_name][sample_name] = {}
                JES_syst_values[channel_name][sample_name] = {}

            print("TOM: testing what the JER_syst_values looks like here: ", JER_syst_values)
            print("TOM: testing what the JMS_syst_values looks like here: ", JMS_syst_values)
            print("TOM: testing what the JES_syst_values looks like here: ", JES_syst_values)

            if sample_name == 'data': continue
            if args.signalSample in sample_name: 
                print(f"TOM: signal is getting group defn {sample_name}")
                syst_group = 'signal'
            elif 'Z_jets' in sample_name:
                syst_group = 'Z_jets'
            elif 'singletop' in sample_name:
                syst_group = 'singletop'
            elif sample_name in ['ttbb', 'ttlight']:
                syst_group = 'ttbar'
            elif 'ttcc' in sample_name:
                syst_group = 'ttcc'
            else:
                syst_group = "bkgd"
            #if "CR" in channel.name:
            # add systematics, from systematics dictionary - only default for now, just btagging, jvt, met_trig
            for syst_name, syst in systematics[syst_group]['default'].items():
                print(f"Adding {syst_name} to {sample_name}")
                if "VR" in channel.name: print("FOR THE VALIDATION!")
                sample.addSystematic(syst)
            
            # also add systematics from 'all' category, met_resoPara, met_reso_perp, met_scale, pileup_rew
            for syst_name, syst in systematics[syst_group]['all'].items():
                print(f"Adding {syst_name} to {sample_name}")
                if "VR" in channel.name: print("FOR THE VALIDATION!")
                sample.addSystematic(syst)
            """else:
                print("Adding systematics to the SR")
                if "SR" in channel.name:
                    for syst_name, syst in systematics['SR']['default'].items():
                        print(f"Adding {syst_name} to {sample_name}")
                        sample.addSystematic(syst)
                    
                    # also add systematics from 'all' category, met_resoPara, met_reso_perp, met_scale, pileup_rew
                    for syst_name, syst in systematics['SR']['all'].items():
                        print(f"Adding {syst_name} to {sample_name}")
                        sample.addSystematic(syst)
            """

            # at JER systematics to the signal (not currently available)
            if args.signalSample in sample_name:
                print("TOM: at the start of JER systematics for signal")
                print("TOM: what sample are we applying JER systs too? ", sample_name)
                if "monoShh" in sample_name: continue
                # Add JER to signal, JES and JMS are done via tree
                unc_dict = json.load(open("jsons/JER/JER_unc_"+sample_name+".json"))
                if channel_name not in unc_dict[sample_name]: print(f"uncertainty not found {channel_name}")

                for syst in JER_systs:
                    variation_up = unc_dict[sample_name].get(channel_name, {syst+"_up": 0.5})[syst+"_up"]
                    variation_down = unc_dict[sample_name].get(channel_name, {syst+"_down": 0.5})[syst+"_down"]
                    #logging.info("Adding {0} unc from JSON to {1} val: up={2}, down={3}".format(syst, channel_name, variation_up, variation_down))
                    JER_syst_values[channel_name][sample_name][syst] = Systematic("JER_"+syst, configMgr.weights, 1+variation_up, 1-variation_down, "user","userNormHistoSys")
                    #sample.addSystematic(JER_syst_values[channel_name][sample_name][syst])

                continue # End the loop for signal here, I don't want to add JER/JES/JMS to the signal like it is done below

            # Adding JER/JMS systematics for backgrounds
            # do completely separate logic for ttbar, as it's funky to handle:
            if "tt" in sample_name:
                sample_ttbar = "ttbar_sys"
                unc_dict_JER = json.load(open("jsons/JER/JER_ttbar_sys.json"))
                unc_dict_JMS = json.load(open("jsons/JMS/JMS_ttbar.json"))
                unc_dict_JES = json.load(open("jsons/JES/JES_ttbar.json"))

                if channel_name not in unc_dict_JER[sample_ttbar]: 
                    print(f"JER uncertainty not found {channel_name}")
                if channel_name not in unc_dict_JMS[sample_ttbar]: 
                    print(f"JMS ncertainty not found {channel_name}")
                for syst in JER_systs:
                    variation_up = unc_dict_JER[sample_ttbar].get(channel_name, {syst+"_up": 0.5})[syst+"_up"]
                    variation_down = unc_dict_JER[sample_ttbar].get(channel_name, {syst+"_down": 0.5})[syst+"_down"]
                    #logging.info("Adding {0} unc from JSON to {1} val:  eup={2}, down={3}".format(syst, channel_name, variation_up, variation_down))
                    if syst_group == 'ttbar':
                        print("TOM: adding normalised systematic in the control regions, effectively doing nothing?")
                        print("Adding {0} unc from JSON to {1} val:  eup={2}, down={3}".format(syst, channel_name, variation_up, variation_down))
                        JER_syst_values[channel_name][sample_ttbar][syst] = Systematic("JER_"+syst, configMgr.weights, 1+variation_up, 1-variation_down, "user","userNormHistoSys")
                    else:
                        print("Adding {0} unc from JSON to {1} val:  eup={2}, down={3}".format(syst, channel_name, variation_up, variation_down))
                        print("TOM: adding shape systematics to both ttcc and to ttbb, ttlight in the signal region")
                        JER_syst_values[channel_name][sample_ttbar][syst] = Systematic("JER_"+syst, configMgr.weights, 1+variation_up, 1-variation_down, "user","userHistoSys")

                    #sample.addSystematic(JER_syst_values[channel_name][sample_ttbar][syst])
                
                for syst in JMS_systs:
                    variation_up = unc_dict_JMS[sample_ttbar].get(channel_name, {syst+"_up": 0.5})[syst+"_up"]
                    variation_down = unc_dict_JMS[sample_ttbar].get(channel_name, {syst+"_down": 0.5})[syst+"_down"]
                    #logging.info("Adding {0} unc from JSON to {1} val:  eup={2}, down={3}".format(syst, channel_name, variation_up, variation_down))
                    print("Adding {0} unc from JSON to {1} val:  eup={2}, down={3}".format(syst, channel_name, variation_up, variation_down))
                    if syst_group == 'ttbar':
                        print("TOM: adding normalised systematic in the control regions, effectively doing nothing?")
                        print("Adding {0} unc from JSON to {1} val:  eup={2}, down={3}".format(syst, channel_name, variation_up, variation_down))
                        JMS_syst_values[channel_name][sample_ttbar][syst] = Systematic("JMS_"+syst, configMgr.weights, 1+variation_up, 1-variation_down, "user","userNormHistoSys")
                    else:
                        JMS_syst_values[channel_name][sample_ttbar][syst] = Systematic("JMS_"+syst, configMgr.weights, 1+variation_up, 1-variation_down, "user","userHistoSys")

                    sample.addSystematic(JMS_syst_values[channel_name][sample_ttbar][syst])
                
                for syst in JES_systs:
                    variation_up = unc_dict_JES[sample_ttbar].get(channel_name, {syst+"_up": 0.5})[syst+"_up"]
                    variation_down = unc_dict_JES[sample_ttbar].get(channel_name, {syst+"_down": 0.5})[syst+"_down"]
                    #logging.info("Adding {0} unc from JSON to {1} val:  eup={2}, down={3}".format(syst, channel_name, variation_up, variation_down))
                    print("Adding {0} unc from JSON to {1} val:  eup={2}, down={3}".format(syst, channel_name, variation_up, variation_down))
                    if syst_group == 'ttbar':
                        print("TOM: adding normalised systematic in the control regions, effectively doing nothing?")
                        print("Adding {0} unc from JSON to {1} val:  eup={2}, down={3}".format(syst, channel_name, variation_up, variation_down))
                        JES_syst_values[channel_name][sample_ttbar][syst] = Systematic("JES_"+syst, configMgr.weights, 1+variation_up, 1-variation_down, "user","userNormHistoSys")
                    else:
                        JES_syst_values[channel_name][sample_ttbar][syst] = Systematic("JES_"+syst, configMgr.weights, 1+variation_up, 1-variation_down, "user","userHistoSys")
                    sample.addSystematic(JES_syst_values[channel_name][sample_ttbar][syst])

            else:
                unc_dict_JER = json.load(open("jsons/JER/JER_"+sample_name+".json"))
                unc_dict_JMS = json.load(open("jsons/JMS/JMS_"+sample_name+".json"))
                unc_dict_JES = json.load(open("jsons/JES/JES_"+sample_name+".json"))


                print("TOM: doing JER systematics for sample: ", sample_name)
                print("TOM: json file that we're loading: jsons/JER_"+sample_name+".json")
                #print(unc_dict_JER)

                if channel_name not in unc_dict_JER[sample_name]:
                    print(f"JER uncertainty not found {channel_name}")
                if channel_name not in unc_dict_JMS[sample_name]:
                    print(f"JMS uncertainty not found {channel_name}")
                
                for syst in JER_systs:
                    variation_up = unc_dict_JER[sample_name].get(channel_name, {syst+"_up": 0.5})[syst+"_up"]
                    variation_down = unc_dict_JER[sample_name].get(channel_name, {syst+"_down": 0.5})[syst+"_down"]
                    #logging.info("Adding {0} unc from JSON to {1} val:  eup={2}, down={3}".format(syst, channel_name, variation_up, variation_down))
                    if sample.name in ["Z_jets"]:
                        print("TOM: adding normalised systematic in the control regions, effectively doing nothing?")
                        print("Adding {0} unc from JSON to {1} val:  eup={2}, down={3}".format(syst, channel_name, variation_up, variation_down))
                        JER_syst_values[channel_name][sample_name][syst] = Systematic("JER_"+syst, configMgr.weights, 1+variation_up, 1-variation_down, "user","userNormHistoSys")
                    else:
                        JER_syst_values[channel_name][sample_name][syst] = Systematic("JER_"+syst, configMgr.weights, 1+variation_up, 1-variation_down, "user","userHistoSys")

                    #sample.addSystematic(JER_syst_values[channel_name][sample_name][syst])
                
                for syst in JMS_systs:
                    variation_up = unc_dict_JMS[sample_name].get(channel_name, {syst+"_up": 0.5})[syst+"_up"]
                    variation_down = unc_dict_JMS[sample_name].get(channel_name, {syst+"_down": 0.5})[syst+"_down"]
                    #logging.info("Adding {0} unc from JSON to {1} val:  eup={2}, down={3}".format(syst, channel_name, variation_up, variation_down))
                    print("Adding {0} unc from JSON to {1} val:  eup={2}, down={3}".format(syst, channel_name, variation_up, variation_down))
                    if sample.name in ["Z_jets"]:
                        print("TOM: adding normalised systematic in the control regions, effectively doing nothing?")
                        print("Adding {0} unc from JSON to {1} val:  eup={2}, down={3}".format(syst, channel_name, variation_up, variation_down))
                        JMS_syst_values[channel_name][sample_name][syst] = Systematic("JMS_"+syst, configMgr.weights, 1+variation_up, 1-variation_down, "user","userNormHistoSys")
                    else:
                        JMS_syst_values[channel_name][sample_name][syst] = Systematic("JMS_"+syst, configMgr.weights, 1+variation_up, 1-variation_down, "user","userHistoSys")
                    sample.addSystematic(JMS_syst_values[channel_name][sample_name][syst])
                
                for syst in JES_systs:
                    variation_up = unc_dict_JES[sample_name].get(channel_name, {syst+"_up": 0.5})[syst+"_up"]
                    variation_down = unc_dict_JES[sample_name].get(channel_name, {syst+"_down": 0.5})[syst+"_down"]
                    #logging.info("Adding {0} unc from JSON to {1} val:  eup={2}, down={3}".format(syst, channel_name, variation_up, variation_down))
                    print("Adding {0} unc from JSON to {1} val:  eup={2}, down={3}".format(syst, channel_name, variation_up, variation_down))
                    if sample.name in ["Z_jets"]:
                        JES_syst_values[channel_name][sample_name][syst] = Systematic("JES_"+syst, configMgr.weights, 1+variation_up, 1-variation_down, "user","userNormHistoSys")
                    else:
                        JES_syst_values[channel_name][sample_name][syst] = Systematic("JES_"+syst, configMgr.weights, 1+variation_up, 1-variation_down, "user","userHistoSys")

                    sample.addSystematic(JES_syst_values[channel_name][sample_name][syst])
        
                
    # now add SR systematics    

    print("TOM: Finished Experimental Systematics!")



def add_theory_systematics(channels):
    print("Started adding theory systematics to backgrounds.")
    # define dictionaries for singletop systematics
    syst_singletop_gen = {}
    syst_singletop_par = {}
    syst_singletop_int = {}
    # ttbar dictionaries
    syst_ttbar_gen = {}
    syst_ttbar_par = {}

    # W and Z scale variation files
    syst_W_scaleVar = {}
    syst_Z_scaleVar = {}

    # load in singletop and ttbar theory systematics
    unc_theo_st        = json.load(open("jsons/Theory_Systs/singleTop/singleTop_Theory.json"))
    unc_theo_ttbar     = json.load(open("jsons/Theory_Systs/ttbar/ttbar_Theory.json"))

    for channel in channels:
        print(f"What channels do we have here? {channel.name}")
        channel_name = channel.name.replace('cuts_','').replace('m_hh_new_min_dR_','').replace('_SR','SR')
        if "SR" not in channel_name: channel_name += "_monoShh"
        if not any(region in channel_name for region in ["CR", "VR", "SR"]):
            print("The channel that we're plotting isnt a valid region!")
            raise ValueError("This doesn't make any sense, this region isn't valid: {channel_name}")
        for sample in channel.sampleList:
            print(f"Sample we're applying theory systematics to {sample.name}")
            if sample.name in ["topEW", "diboson", "VH", "triboson"]:
                print(f"adding 20% uncertainty to {sample.name}, conservative estimation of theory systematics")
                sample.addSystematic(Systematic(f"{sample.name}_syst_{channel_name}", configMgr.weights, 1.2, 0.8, "user", "userOverallSys"))
            elif "tt" in sample.name:
                print("Adding theory uncertainties to ttbar")
                #sample.addSystematic(systematics['ttbar']['theo']['muR_syst'])
                #sample.addSystematic(systematics['ttbar']['theo']['muF_syst'])
                #sample.addSystematic(systematics['ttbar']['theo']['Var3c_syst'])
            elif "singletop" in sample.name:
                print("Adding theory uncertainties to singletop")
                #sample.addSystematic(systematics['singletop']['theo']['muR_syst'])
                #sample.addSystematic(systematics['singletop']['theo']['muF_syst'])
                #sample.addSystematic(systematics['singletop']['theo']['Var3c_syst'])
            elif sample.name in ["W_jets", "Z_jets"]:
                print(f"Adding theory uncertainties to {sample.name}")
                #sample.addSystematic(systematics[sample.name]['theo']['WZ_muR_syst'])
                #sample.addSystematic(systematics[sample.name]['theo']['WZ_muF_syst'])
                #sample.addSystematic(systematics[sample.name]['theo']['WZ_muR_muF_syst'])
            if sample.name == 'singletop':
                if channel_name not in unc_theo_st: print(f"Ahh singletop uncertainties not found for {channel_name}")

                # now we do this for singletop systematics (for now, missing PS) 
                variation = unc_theo_st[sample.name].get(channel_name, {'Generator': 0.5})['Generator']
                print(f"Adding generator systematic to singletop at {channel_name}, variation: {variation}")
                syst_singletop_gen[channel_name] = Systematic("singletop_gen_syst", configMgr.weights, 1+variation, 1-variation, "user", "userHistoSys")
                channel.getSample("singletop").addSystematic(syst_singletop_gen[channel_name])

                # within these jsons, interference = inference
                variation = unc_theo_st[sample.name].get(channel_name, {'Inference':0.5})['Inference']
                print(f"Adding interference systematic to singletop at {channel_name}, variation: {variation}")
                syst_singletop_int[channel_name] = Systematic("singletop_int_syst", configMgr.weights, 1+variation, 1-variation, "user", "userHistoSys")
                channel.getSample("singletop").addSystematic(syst_singletop_int[channel_name])

                variation = unc_theo_st[sample.name].get(channel_name, {'Hadronisation':0.5})['Hadronisation']
                print(f"Adding parton shower systematic to singletop at {channel_name}, variation: {variation}")
                syst_singletop_par[channel_name] = Systematic("singletop_par_syst", configMgr.weights, 1+variation, 1-variation, "user", "userHistoSys")
                channel.getSample("singletop").addSystematic(syst_singletop_par[channel_name])
            if 'tt' in sample.name:
                if channel_name not in unc_theo_ttbar: print(f"Ahh ttbar uncertainties not found for {channel_name}")

                # we're also adding scale variation systematics as json contributions now?
                #variation
                # additional variations for ttbar and singletop
                # it looks like we're missing parton shower for singletop at the moment
                # first for ttbar: parton shower & generator
                variation = unc_theo_ttbar['ttbar'].get(channel_name, {'PS': 0.5})['PS']
                print(f"Adding ttbar parton shower systematic from JSON to {channel_name}, variation: {variation}")
                if 'ttcc' in sample.name or 'SR' in channel_name:
                    print(f"Adding ttcc parton shower systematic as non-normalised systematic uncertainty to {sample.name}")
                    syst_ttbar_par[channel_name] = Systematic("ttbar_PS_syst", configMgr.weights, 1+variation, 1-variation, "user", "userHistoSys")
                else:
                    syst_ttbar_par[channel_name] = Systematic("ttbar_PS_syst", configMgr.weights, 1+variation, 1-variation, "user", "userNormHistoSys")
                # add separately for each ttbar sub-category
                channel.getSample(sample.name).addSystematic(syst_ttbar_par[channel_name])

                #channel.getSample(sample.name).addSystematic(syst_ttbar_par[channel_name])
                #channel.getSample("ttbb").addSystematic(syst_ttbar_par[channel_name])
                #channel.getSample("ttcc").addSystematic(syst_ttbar_par[channel_name])

                # now for ttbar generator systematic
                variation = unc_theo_ttbar['ttbar'].get(channel_name, {'Generator': 0.5})['Generator']
                print(f"Adding ttbar generator uncs to {channel_name}, variation: {variation}")
                if 'ttcc' in sample.name or 'SR' in channel_name:
                    print(f"Adding ttcc parton shower systematic as non-normalised systematic uncertainty to {sample.name}")
                    syst_ttbar_gen[channel_name] = Systematic("ttbar_gen_syst", configMgr.weights, 1+variation, 1-variation, "user", "userHistoSys")
                else:
                    print(f"Adding ttbar normalised generator systematic to {channel_name}")
                    syst_ttbar_gen[channel_name] = Systematic("ttbar_gen_syst", configMgr.weights, 1+variation, 1-variation, "user", "userNormHistoSys")
                # again, add separately for each ttbar sub-category
                channel.getSample(sample.name).addSystematic(syst_ttbar_gen[channel_name])
                #channel.getSample("ttlight").addSystematic(syst_ttbar_gen[channel_name])
                #channel.getSample("ttbb").addSystematic(syst_ttbar_gen[channel_name])
                #channel.getSample("ttcc").addSystematic(syst_ttbar_gen[channel_name])


print(dataFiles)

dataSample = Sample("data",getSampleColor("data"))
dataSample.addInputs(dataFiles)
dataSample.setData()

#**************
# Background Fit
#**************
useStat = True
fitConfig = configMgr.addFitConfig("FitConfig")
fitConfig.statErrThreshold = 0.05 if useStat else None

print(regions)
for region in regions:
    print(region)        
    if region not in configMgr.cutsDict:
        print("Where is the region? ", region)

    if ('CR' in region) or ('VR' in region and configMgr.myFitType == configMgr.FitType.Background):
        print(f'adding region {region} to fit configuration, for CR we only add single-bin CR regions')
        channel = fitConfig.addChannel("cuts", [region], 1, 0.5, 1.5)
        channel.hasB = False
        channel.hasBQCD = False
        channel.useOverflowBin = False

        if 'CR' in region:
            print("Adding CR as control region")
            fitConfig.addBkgConstrainChannels([channel])
        elif 'VR' in region:
            print("Adding VR as validation region")
            fitConfig.setValidationChannels([channel])
    elif 'SR' in region:
        # some funky chatgpt to get the bin values :) (from strings such as SR_monoShh_bin_50_100)
        regex_pattern = r'\d+'
        matches = re.findall(regex_pattern, region)

        if len(matches) == 2:
            bin_lower = float(matches[0])
            bin_upper = float(matches[1])
            print(f"Great, we've found two numbers from the string - hopefully upper {bin_upper} and lower {bin_lower} bin edges")

        print("Now adding SR to fit configuration as a binned mhh distribution")
        channel = fitConfig.addChannel("m_hh_new_min_dR", [region], 1, bin_lower, bin_upper)
        channel.hasB = False
        channel.hasBQCD = False
        channel.useOverflowBin = False
        if configMgr.myFitType == configMgr.FitType.Background:
            print("Adding signal region as validation region")
            fitConfig.setValidationChannels([channel])
            channel.blind = True
        else:
            print("Adding signal region as signal region")
            print(channel)
            fitConfig.addSignalChannels([channel])
            # we also blind this region
            print("Blinding signal region")
            channel.blind = True


#------------------------
# list of data/MC plots
#------------------------
if doPlots:

  print("TOM: fitConfig.channels ", fitConfig.channels)
  print("TOM: names ", [c.name for c in fitConfig.channels])
  channel_names = [c.name.replace('cuts_', '').replace('m_hh_new_min_dR_','') for c in fitConfig.channels]

  for channel_name in channel_names:      
    #if "VR" not in channel_name: continue  
    plot_configs = [{"name": "mTb_min", "nbins": 5, "min": 100, "max": 350, "titleX": "m_{T}^{min} (b-jets,E_{T}^{miss}) [GeV]", "maxY": 90.},
                    {"name": "ZCR_mTb_min", "nbins": 5, "min": 100, "max": 350, "titleX": "\mu \text{corrected} m_{T}^{min} (b-jets,E_{T}^{miss}) [GeV]", "maxY": 70.},
                    {"name": "bjets_n", "nbins": 4, "min": 2.5, "max": 6.5, "titleX": "Number of b-jets with p_{T} > 40 GeV", "maxY": 100.},
                    {"name": "jets_n", "nbins": 5, "min": 3.5, "max": 8.5, "titleX": "Number of jets", "maxY": 80.},
                    #{"name": "m_hh_new_min_dR", "nbins": 4, "min": 50, "max": 950, "titleX": "Di-Higgs Mass [GeV]", "maxY": 100.},
                    {"name": "met", "nbins": 6, "min": 200, "max": 800, "titleX": "E_{T}^{miss} [GeV]", "maxY": 100.},
                    {"name": "ZCR_met", "nbins": 6, "min": 200, "max": 800, "titleX": "\mu \text{corrected E_{T}^{miss} [GeV]", "maxY": 100.},
                    #{"name": "NNScore", "nbins": 1, "min": 0.85, "max": 0.9, "titleX": "Neural Network Score", "maxY": 200.}
                    {"name": "metsig_obj", "nbins": 5, "min": 0, "max": 20, "titleX": "\sigma(E_{T}^{miss})", "maxY": 100.},
                    {"name": "ZCR_metsig_estimate", "nbins": 5, "min": 0, "max": 20, "titleX": "\mu \text{corrected \sigma(E_{T}^{miss})}", "maxY": 100.}
                    ]
    channel_specifics_ymax = {"SR": 1., "CR_4b": 1.2, "CR_3b_high_mTb": 2., "CR_3b_low_mTb": 8., "CR_Z": 2., "VR": 0.6}

    for pc in plot_configs:
      if channel_name == "CR_3b_high_mTb" and pc['name'] == "mTb_min":
        plot = fitConfig.addChannel(pc['name'], [channel_name], 3, 200., pc['max'])
      else:
        plot = fitConfig.addChannel(pc['name'], [channel_name], pc['nbins'], pc['min'], pc['max'])
    
      plot.titleX = pc['titleX']
      plot.minY = 0.5
      plot.logY = False
      if "Presel" in channel_name:
        plot.maxY = 1e5
        plot.logY = True

      
      plot.maxY = pc["maxY"] * channel_specifics_ymax[channel_name]

      plot.titleY = "Events"
      plot.ATLASLabelX = 0.12
      plot.ATLASLabelY = 0.91
      plot.ATLASLabelText = "Preliminary"
      plot.showLumi = True
      plot.lumi = 140
      plot.RemoveEmptyBins = True

      fitConfig.setValidationChannels([plot])

# we also need to add the background and data samples to this fit configuration: (do we need to add signal here too? I guess not as this is for the background only fit)
print("BACKGROUND SAMPLES .VALUES" ,bkg_samples.values())
print(bkg_samples)
for sample in bkg_samples.values():
    fitConfig.addSamples([sample])
fitConfig.addSamples([dataSample])

meas=fitConfig.addMeasurement(name="NormalMeasurement",lumi=1.0,lumiErr=0.0083)
meas.addPOI("mu_SIG")
# add flat uncs to signal and control regions
flatUnc = False
if flatUnc:
  for channel in fitConfig.channels:
    channel_name = channel.name.replace('cuts_','').replace('m_hh_new_min_dR_','')

    #if "CR" in channel_name: continue
    if flatUnc:
      unc_up, unc_down = 1.5, 0.5
      print("TOM: doing flat 50 uncs")
    #if args.doFlatUnc50:
    #  unc_up, unc_down = 1.5, 0.5
    #  print("TOM: doing flat 50 uncs")

    for sample in channel.sampleList:
      print("TOM: sample,name",sample.name)
      sample.addSystematic(Systematic(sample.name+"_syst_"+channel_name, configMgr.weights, unc_up, unc_down,"user","userOverallSys"))
  
#**************
# Discovery Fit
#**************

if configMgr.myFitType==configMgr.FitType.Discovery:
 
   #Fit config instance
   discoveryFitConfig = configMgr.addFitConfig("Discovery")
   meas=discoveryFitConfig.addMeasurement(name="NormalMeasurement",lumi=1.0,lumiErr=0.039)
   meas.addPOI("mu_Discovery")
 
   #Samples
   discoveryFitConfig.addSamples([topSample,wzSample,dataSample])

   #Systematics
   discoveryFitConfig.getSample("Top").addSystematic(topKtScale)
   discoveryFitConfig.getSample("WZ").addSystematic(wzKtScale)
   discoveryFitConfig.addSystematic(jes)

   #Channel
   srBin = discoveryFitConfig.addChannel("cuts",["SR"],1,0.5,1.5)
   discoveryFitConfig.addSignalChannels([srBin])
   srBin.addDiscoverySamples(["Discovery"],[1.],[0.],[10000.],[kMagenta])

print("TOM: myFitType: ", configMgr.myFitType)
# writing some logic to double check the type of fit:
if configMgr.FitType.Exclusion == configMgr.myFitType:
    print("Tom: We're excluding something!")
else:
    print("Tom: does this mean we're being inclusive?")
doTheoSyst = False


#**************
# Exclusion fit
#**************
if configMgr.myFitType==configMgr.FitType.Exclusion:
    print("TOM: running exclusion fit")
    # Fit config instance
    #FitConfig = configMgr.addFitConfigClone(fitConfig, "Exclusion")
    Xsect_down = replaceWeight(configMgr.weights,"weight_lumi_real","weight_lumi_down")
    #Xsect_down = replaceWeight(configMgr.weights,"weight_lumi","weight_lumi_down")
    Xsect_up = replaceWeight(configMgr.weights,"weight_lumi_real","weight_lumi_up")
    print(sigFiles)
    sigSample = Sample(mass, kPink)
    sigSample.addSystematic(Systematic("SigXSec", configMgr.weights, Xsect_up, Xsect_down, "weight", "overallSys" ))

    sigSample.addInputs(sigFiles)
    sigSample.setNormByTheory()
    sigSample.setStatConfig(True)
    sigSample.additionalCuts = "((hh_type == 10) || (hh_type == 11) || (hh_type == 12))"
    sigSample.setNormFactor("mu_SIG",1.,0.,5.)                    
    fitConfig.addSamples([sigSample])
    fitConfig.setSignalSample(sigSample)


    # Add theory uncertainties 
    dosigTheo = False                                                                                                                                                  
    if dosigTheo:
        signalSample = args.signalSample
        massPoint = signalSample.split("_")[1]
        unch_teo_signal = json.load(open("metadata/signal_theoUnc_"+massPoint+".json"))
        signalVariations = ["sc", "qc", "Var1", "Var2", "Var3a", "Var3b", "Var3c"]
        syst_theory_sig = {}

        print("Setting up theory systematics for the signal!")

        for channel in fitConfig.channels:
            channel_name = channel.name.replace('cuts_','').replace('m_hh_new_min_dR_','')
            syst_theory_sig[channel_name] = {}

            # Apply only to SRs                                                                                                                                                       
            if "SR" not in channel_name: 
                continue

            for var in signalVariations:

                print("CARLOS:", channel_name, var)
                if "Var" in var:
                    varName_up = var+"Up"
                    varName_down = var+"Down"
                else:
                    varName_up = var+"up"
                    varName_down = var+"down"
                    variation_up = unch_teo_signal.get(channel_name, {varName_up: 0.5})[varName_up]
                    variation_down = unch_teo_signal.get(channel_name, {varName_down: 0.5})[varName_down]
                    print("Adding signal {0} unc from JSON to {1} val: {2}".format(var, channel_name, var))
                    syst_theory_sig[channel_name][var] = Systematic("signal_thUnc_"+var, configMgr.weights, 1+variation_up, 1+variation_down, "user","userHistoSys")
                    channel.getSample(signalSample).addSystematic(syst_theory_sig[channel_name][var])

# add systematics for all types of fit! 
add_experimental_systematics(fitConfig.channels)

# adding theory systematics to backgrounds
add_theory_systematics(fitConfig.channels)

end_time = time.time()
elapsed_time = end_time - start_time
print("TOM: overall time: ", elapsed_time)
