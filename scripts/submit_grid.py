#!/usr/bin/env python3
import os
from argparse import ArgumentParser

import uproot


def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('-I', '--input_sigfile', default="/data/atlas/users/bruckler/production_21p2p129/filtered_newGrid/Sig_21.2.129_mc16a.monoShh_newGrid_allRounds_optimisedSelectionFiltered_defined10_ID_withNNScore_4096_disCo_train_set_and_additional_events_allb_inclZCR_monoShhall_allSignals.root", type=str,
                        help='input file, used to populate a mass-array from the signal grid')
    parser.add_argument('-F', '--fit_type', default="excl", help="What type of fit do you want to run?")
    parser.add_argument('-S', '--do_submit', default=True, help="True/False, do you want to submit the condor jobs too?")

    return parser.parse_args()

def make_array(mass_points):
    input_array = args.mass_array.split(",")

    return input_array

def make_mass_array(input_file):
    # Open the ROOT file
    root_file = uproot.open(input_file)

    # Get the list of keys (trees) in the root file
    tree_names = root_file.keys()
    #print(tree_names)
    tree_names = list(root_file.keys())

    # Define the output file name
    output_file = 'tree_names.txt'
    mass_points = []
    #mass_points = sorted(list(set([name.split("_")[0]+"_"+name.split("_")[1]+"_"+name.split("_")[2]+"_"+name.split("_")[3]+"_"+name.split("_")[4] for name in tree_names if (name.split("_")[4] == nominal)])))
    for name in tree_names:
        #print(name.split("_")[4])

        # Split the name by underscore and check the fifth element against the 'nominal' value
        if name.split("_")[4] == "nominal;1":
            #print(name.split("_")[4])
            # Concatenate the first five elements using underscores and add it to mass_points
            mass_point = "_".join(name.split("_")[:5])
            mass_points.append(mass_point)
    
    mass_points = [s.replace('_nominal;1', '') for s in mass_points]

    for string in mass_points:
        #print('"'+string+'"')
        print(string)
    
    return mass_points
    
def generate_condor_script(mass_point, fit_type):
    script = f'''\
    executable = scripts/HistFitter_mass.sh
    arguments = {mass_point} {fit_type}
    error = condor/error/{mass_point}_{fit_type}_asimov_newsamples.err
    output = condor/output/{mass_point}_{fit_type}_asimov_newsamples.out
    log = condor/log/{mass_point}_{fit_type}_asimo_newsamples.log
    queue
    '''

    # Create a directory for logs if it doesn't exist
    os.makedirs("logs", exist_ok=True)

    # Write the HTCondor submission script to a file
    with open(f"submit_{mass_point}.submit", "w") as file:
        file.write(script)

def submit_condor_jobs(mass_points):
    for mass_point in mass_points:
        submit_file = f"submit_{mass_point}.submit"
        os.system(f"condor_submit {submit_file}")



def main(mass_points, fit_type):
    for mass_point in mass_points:
        generate_condor_script(mass_point, fit_type)

if __name__ == "__main__":
    args = get_args()
    # read input arguments: submit True/False, mass point array, type of fit to run:
    do_submit = args.do_submit
    print("TOM: submit? ", do_submit)
    input_file = args.input_sigfile
    mass_points = make_mass_array(input_file)
    # Remove the ';1' from each string in the list
    #modified_list = [s.replace('_nominal;1', '') for s in mass_points]

    # Print the modified list
    for item in mass_points:
        print(item)
    print("TOM: mass array to perform fits for , ", mass_points)
    fit_type = args.fit_type
    print("TOM: fit type, ", fit_type)
    # setup condor_submit scripts
    main(mass_points, fit_type)
    # submit scripts, if user wants to
    if do_submit:
        print("Submitting :)")
        submit_condor_jobs(mass_points)

