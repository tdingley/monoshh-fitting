#!/bin/bash
# setup environment on the batch system
# setupATLAS:
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh
export directory="/data/atlas02/users/dingleyt/monoShh/test_HF/monoshh-fitting"

# setup HistFitter
export curr_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo $curr_dir
#cd $directory
lsetup "views LCG_104a x86_64-el9-gcc12-opt"
source install/bin/setup_histfitter.sh
echo $directory
# which mass-point are we currently running
echo "Running with mass: ${1}"
pwd


#HistFitter.py -w -f -p -L "INFO" -F excl python/multipleSRs_monoShh.py ++signalSample monoShhall_zp1700_dm200_dh310 ++doToys ++diffCR > logs/condor_test.log 2> logs/condor_test.err


# if statement logic to run toys or asymptotics
if [ "${4}" = 'bkg' ]; then
    if [ "${5}" = 'True' ]; then
        HistFitter.py -w -f -L "INFO" -F bkg -D "before,after,corrMatrix" python/multipleSRs_monoShh.py ++signalSample ${1} ++doSyst ++addString "FitTests" ++diffCR > logs/background_${1}_withSyst${3}_diffCR${5}.log 2>  logs/background_${1}_withSyst${3}_diffCR${5}.err
    else
        HistFitter.py -w -f -L "INFO" -F bkg -D "before,after,corrMatrix" python/multipleSRs_monoShh.py ++signalSample ${1} ++doSyst ++addString "" > logs/background_${1}_withSyst${3}_diffCR${5}.log 2>  logs/background_${1}_withSyst${3}_diffCR${5}.err
    fi
else
    if [ "${2}" = 'True' ]; then
        if [ "${3}" = 'True' ]; then
            HistFitter.py -w -f -l -a -p -L INFO -F excl -D "before,after,corrMatrix" python/multipleSRs_monoShh.py ++signalSample ${1} ++doToys ++doSyst > logs/excl_${1}_withSyst${3}.log 2>  logs/excl_${1}_withSyst${3}.err
        else
            HistFitter.py -w -f -l -a -p -L INFO -F excl python/multipleSRs_monoShh.py ++signalSample ${1} ++doToys ++addString "TestCondorToys" >logs/excl_${1}_withSyst${3}.log 2>  logs/excl_${1}_withSyst${3}.err
        fi
    else
        if [ "${3}" = 'True' ]; then
            HistFitter.py -w -f -a -p -L INFO -F excl -D "before,after,corrMatrix" python/multipleSRs_monoShh.py ++signalSample ${1} ++doSyst ++doPlots > logs/excl_${1}_withSyst${3}.log 2>  logs/excl_${1}_withSyst${3}.err
        else
            HistFitter.py -w -f -a -p -L INFO -F excl -D "before,after,corrMatrix" python/multipleSRs_monoShh.py ++signalSample ${1} ++addString "TestCondorAsymptotics" > logs/excl_${1}_withSyst${3}.log 2>  logs/excl_${1}_withSyst${3}.err
        fi
    fi
fi