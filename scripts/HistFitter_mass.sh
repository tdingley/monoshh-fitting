#!/bin/bash
# setup environment on the batch system
# setupATLAS:
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh
export directory="/data/atlas02/users/dingleyt/monoShh/test_HF/monoshh-fitting"

# setup HistFitter
export curr_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $directory

lsetup "views LCG_104a x86_64-el9-gcc12-opt"
source $directory/install/bin/setup_histfitter.sh
echo $directory
# which mass-point are we currently running
echo "Running with mass: ${1}"
pwd


HistFitter.py -p -f -a -w -F ${2} python/multipleSRs_monoShh.py ++signalSample ${1} ++doSyst ++addString "GRID" > $directory/logs/grid_excl_${1}_withSyst.log 2>  $directory/logs/grid_excl_${1}_withSyst.err
