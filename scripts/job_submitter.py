import os
import time
from argparse import ArgumentParser


def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('-m', '--mass_point', default='monoShhall_zp1700_dm200_dh310', help='Please specify in the form monoShhall_zpX_dm200_dh_Y')
    parser.add_argument('-T', '--do_toys', default='False', help='Please specify True for running toys and False for asymptotics')
    parser.add_argument('-d', '--dry_run', default='False', help='You can do a dry-run in which we do not submit the condor file')
    parser.add_argument('-S', '--do_expsyst', default='False', help='Please specify True for running exp_syst, make sure to change this within config_1, false for flat50')
    parser.add_argument('-F', '--fit_type', default="bkg")
    parser.add_argument('-C', '--do_diffCR', default='False')
    return parser.parse_args()


args = get_args()
mass = args.mass_point
toys = args.do_toys
dry_run = args.dry_run
exp_syst = args.do_expsyst
fit_type = args.fit_type
diffCR = args.do_diffCR

print('Generating script corresponding to mass point '+ str(mass)+ ', and are we running toys? ' + str(toys) + ' are we running experimental systematics? ', exp_syst)
print(f'Are we running with alternative control regions? {diffCR}')
job_file = "HistFitter_" + str(mass) + "_doToys_" + str(toys) + "_doExpSyst_" + exp_syst+ "fitType" +str(fit_type)+ ".submit"
print('File Name: ', str(job_file))

with open(job_file, 'w') as f:
        f.write('executable = scripts/condor_executable.sh\n')
        f.write('arguments = ' + str(mass) + " " + str(toys) +" "+ str(exp_syst) + " " + str(fit_type)+ " " + str(diffCR) +'\n')
        f.write('output = condor/output/output_' +str(mass) + "_doToys_"+str(toys)+ "_" + str(fit_type)+ " " + str(diffCR)+ '.out\n')
        f.write('error = condor/error/error_' +str(mass) + "_doToys_"+str(toys)+'doExpSyst'+str(exp_syst)+ "_" + str(fit_type)+"_" + str(diffCR)+  '.err\n')
        f.write('log = condor/log/log_'+str(mass) + "_doToys_"+str(toys)+'doExpSyst'+str(exp_syst)+ "_" + str(fit_type)+ "_" + str(diffCR)+ '.log'+'\n')
        f.write('initialdir = /data/atlas/users/dingleyt/monoShh/test_HF/monoshh-fitting' + '\n')
        f.write('queue' + '\n')

if dry_run == "True":
    print("Dryrun, not submitting file - we are finished - adios!")
else:
    print("Now submitting this condor file")
    os.system('condor_submit ' + job_file)
    print("To check it has worked, we wait a few seconds then do a condor_q")
    time.sleep(5)
    os.system('condor_q')
    print("Finished - adios!")
